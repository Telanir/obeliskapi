# Obelisk #

Obelisk is a macroplugin that acts as a manager to content-containing modules. It acts as a solid library and foundation to [Lord of the Craft](http://www.lordofthecraft.net) and [MineGrid](http://www.minegrid.net) in particular, and is responsible for keeping the plugin ecosystem functioning and healthy.