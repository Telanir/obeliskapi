package net.minegrid.obelisk.api.blocks;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.event.EventPriority;
import org.bukkit.inventory.ItemStack;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.blocks.events.ObeliskBlockBreakEvent;
import net.minegrid.obelisk.core.blocks.BlockManager;
import net.minegrid.obelisk.core.blocks.CoreBlock;

/**
 * The {@code OBlock} is the main interface to the {@code Foundation} custom-block API.
 * All values and custom blocks persist across sessions are tied to worlds and chunks.
 * 
 * @author Maxim
 * @version 1.0
 * @since 1.0.0
 */
public interface OBlock {
	/**
	 * @return if true, this block will decay by natural means.
	 */
	boolean doesDecay();
	/**
	 * @return if true, this block can be exploded and burned
	 */
	boolean getDestructible();
	/**
	 * @return if true, this block will not fall or be pushed by pistons.
	 */
	boolean doesIgnorePhysics();
	/**
	 * @return if true, this block will not power or be powered by redstone.
	 */
	boolean doesIgnoreRedstone();
	/**
	 * @return returns the Xp drop for the block.
	 */
	int getXpDrop();
	/**
	 * Get items dropped by this custom-block.
	 * @return empty stack if there are no items.
	 */
	ItemStack[] getDrops();
	/**
	 * @return if true, this block will drop items on destruction.
	 */
	boolean hasDrops();

	void setDecays(boolean b);
	void setDestructible(boolean b);
	void setIgnoresPhysics(boolean b);
	void setIgnoresRedstone(boolean b);
	void setXpDrop(int i);
	void setItemDrops(ItemStack...drops);
	
	/**
	 * Removes all data associated to a tag.
	 * @param tag Data to delete.
	 */
	void deleteData(String tag);
	/**
	 * Assigns new data to be saved.
	 * @param tag Identifying tag.
	 * @param o Data to store.
	 */
	void setData(String tag, Object o);
	/**
	 * Whether or not data exists for the specified tag.
	 * @param tag Identifying tag.
	 * @return true if data exists for the tag.
	 */
	boolean hasData(String tag);
	/**
	 * Returns stored data.
	 * @param tag Identifying tag.
	 * @return null if there is no data stored.
	 */
	<T> T getData(String tag);
	
	/**
	 * Returns the location of the custom-block.
	 * @return Location in the world of the block.
	 */
	Location getLocation();
	
	/**
	 * Registers this custom-block in the database as a new block.
	 * If a block already exists, this method fails.
	 * <i>Do not register a new custom-block if one already exists.</i>
	 * @return true if registration was successful.
	 */
	boolean register();
	
	/**
	 * Returns an {@code OBlock.Builder} for the location.
	 * <i>Do not use this method if the location already has a custom block.</i>
	 * @param loc Location of block.
	 * @return Builder with location.
	 */
	public static Builder form(Location loc) {
		return new Builder(loc);
	}

	/**
	 * Returns an {@code OBlock.Builder} for the block.
	 * <i>Do not use this method if the block already has custom-data.</i>
	 * @param b Block to use.
	 * @return Builder with block-location.
	 */
	public static Builder form(Block b) {
		return new Builder(b);
	}
	
	/**
	 * Registers an event handler that will execute each time a custom Obelisk
	 * block is broken.
	 * @param m module handling the break
	 * @param ep priority of the event, {@code EventPriority.LOWEST} events run first
	 * @param e handler to execute
	 */
	public static void registerBreakHandler(Module m, EventPriority ep, ObeliskBlockBreakEvent e) {
		BlockManager.getDaemon().registerEvent(m, ep, e);
	}

	/**
	 * Returns whether or not the specified location is represented in the Obelisk framework.
	 * @param loc location to check
	 * @return false if there is no data
	 */
	public static boolean isCustom(Location loc) {
		return BlockManager.isCustom(loc);
	}

	/**
	 * Returns whether or not the specified block is represented in the Obelisk framework.
	 * @param b block to check
	 * @return false if there is no data
	 */
	public static boolean isCustom(Block b) {
		return BlockManager.isCustom(b);
	}

	/**
	 * Returns the data associated to the specified block.
	 * @param b block to retrieve data for
	 * @return null if no data exists for the block
	 */
	public static OBlock getBlock(Block b) {
		return BlockManager.getBlock(b);
	}

	/**
	 * Returns the data associated to a block at the specified location.
	 * @param l location to retrieve data for
	 * @return null if no data exists for the block
	 */
	public static OBlock getBlock(Location l) {
		return BlockManager.getBlock(l);
	}

	/**
	 *
	 * @param b
	 * @return
	 */
	public static OBlock setCustom(Block b) {
		return BlockManager.register(b);
	}
	
	public static OBlock setCustom(Location l) {
		return BlockManager.register(l);
	}
	
	public static void unregister(Block b) {
		BlockManager.unregister(b);
	}
	
	public static void unregister(Location l) {
		BlockManager.unregister(l);
	}
	
	public static class Builder {
		OBlock o;
		
		public Builder(Location loc) {
			o = new CoreBlock(loc);
		}
		
		public Builder(Block b) {
			this(b.getLocation());
		}
		
		public Builder setData(String tag, Object value) {
			o.setData(tag, value);
			return this;
		}
		
		public Builder nonDecaying() {
			o.setDecays(true);
			return this;
		}
		
		public Builder ignoresPhysics() {
			o.setIgnoresPhysics(true);
			return this;
		}
		
		public Builder ignoresRedstone() {
			o.setIgnoresRedstone(true);
			return this;
		}
		
		public Builder indestructible() {
			o.setDestructible(false);
			return this;
		}
		
		public Builder setXpDrop(int i) {
			o.setXpDrop(i);
			return this;
		}
		
		public Builder setItemDrops(ItemStack...is) {
			o.setItemDrops(is);
			return this;
		}
		
		public static Builder start(Block b) {
			return new Builder(b);
		}
		
		public static Builder start(Location l) {
			return new Builder(l);
		}
		
		public OBlock build() {
			return o;
		}
		
		public OBlock register() {
			return o.register() ? o : null;
		}
	}
}
