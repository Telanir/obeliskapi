package net.minegrid.obelisk.api.blocks.events;

public interface ObeliskBlockBreakEvent {
	void process(BlockBreakData data);
}
