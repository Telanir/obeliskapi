package net.minegrid.obelisk.api.blocks.events;

import java.util.Arrays;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import net.minegrid.obelisk.api.blocks.OBlock;

/**
 * This event is called every time a custom block is broken.
 * 
 * @author Maxim
 * @since 1.0.0
 * @version 1.0
 */
public class BlockBreakData {
	Player player;
	BreakType type;
	OBlock custom;
	List<ItemStack> drop;
	Block block;
	boolean cancelled = false;
	Material m;
	byte data;
	
	public BlockBreakData(Player player, Block block, OBlock custom, BreakType type, ItemStack[] drop) {
		this.block = block;
		this.custom = custom;
		this.type = type;
		this.drop = Arrays.asList(drop);
		this.player = player;
		this.m = Material.AIR;
		this.data = 0;
	}
	
	/**
	 * Instead of <code>Material.AIR</code> the block will
	 * be set to the following parameters.
	 * @param m block type
	 * @param data block data, 0 is the default
	 */
	public void setBlockTo(Material m, int data) {
		this.m = m;
		this.data = (byte)data;
	}
	
	/**
	 * Returns the new type the block will
	 * be assigned.
	 */
	public Material getAssignedType() {
		return m;
	}
	
	/**
	 * Returns the new data the block will
	 * be assigned.
	 */
	public byte getAssignedData() {
		return data;
	}
	
	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancel) {
		cancelled = cancel;
	}
	
	/**
	 * Returns the player who broke the block.
	 * @return null if break type is not <code>BreakType.PLAYER</code>
	 */
	public Player getPlayer() {
		return player;
	}
	
	/**
	 * Returns the way the block was destroyed.
	 * @return <code>BreakType.PLAYER</code> if a player intentionally breaks it.
	 */
	public BreakType getType() {
		return type;
	}
	
	/**
	 * Returns the OBlock associated with the <code>Block</code>.
	 */
	public OBlock getData() {
		return custom;
	}
	
	/**
	 * Returns the Bukkit <code>Block</code> that is being broken.
	 */
	public Block getBlock() {
		return block;
	}
	
	/**
	 * Returns the ItemStack's that will be dropped after
	 * the block is destroyed.
	 * @return mutable list, clear to remove all drops.
	 */
	public List<ItemStack> getDrops() {
		return drop;
	}
	
	/**
	 * BreakType indicates the way the block was broken.
	 * Player - broken by a human entity.
	 * Physics - block is trying to fall
	 * Decay - in some way the block has naturally broken
	 * Explode - broken by an explosion.
	 * 
	 * @author Maxim
	 * @since 1.0.0
	 * @version 1.0
	 */
	public enum BreakType {
		PLAYER, PHYSICS, BURN, EXPLODE, DECAY;
	}
}
