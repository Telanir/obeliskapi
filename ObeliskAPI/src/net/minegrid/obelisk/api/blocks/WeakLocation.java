package net.minegrid.obelisk.api.blocks;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;

/**
 * WeakLocations are objects that can be saved using the {@code XStream API}.
 * They are loose-wrappers for Bukkit-Locations.
 * 
 * @author Maxim
 * @since 1.0.0
 * @version 1.0
 */
public final class WeakLocation {
	String world;
	int x, y, z;
	transient Location lc;

	public WeakLocation(String world, int x, int y, int z) {
		this.x = x; this.y = y; this.z = z;
		this.world = world;
	}

	public WeakLocation(Location loc) {
		world = loc.getWorld().getName();
		x = loc.getBlockX();
		y = loc.getBlockY();
		z = loc.getBlockZ();
	}
	
	public int getZ() {
		return z;
	}
	
	public int getY() {
		return y;
	}
	
	public int getX() {
		return x;
	}
	
	public String getWorld() {
		return world;
	}
	
	public Block getBlock() {
		return getLocation().getBlock();
	}
	
	@Override
	public int hashCode() {
		int result = 7;
		result += x*5;
		result += y*11;
		result += z*13;
		for(char c : world.toCharArray())
			result += Character.getNumericValue(c)*3;
		return result;
	}
	
	@Override
	public boolean equals(Object o) {
		if(o instanceof WeakLocation) {
			WeakLocation loc = (WeakLocation) o;
			return (x == loc.x && y == loc.y && z == loc.z && world.equals(loc.world));
		} else
			return false;
	}

	public Location getLocation() {
		if(lc == null)
			lc = new Location(Bukkit.getWorld(world), x, y, z);
		return lc;
	}
	
	public String toString() {
		return x+" "+y+" "+z+" "+world;
	}
}
