package net.minegrid.obelisk.api.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@code OCmdMod} will apply class-wide modifiers to all {@code OCmd}'s.
 * 
 * @author Maxim Chipeev
 * @since 1.0.0
 * @version 1.0
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface OCmdMod {
	//boolean playerOnly() default false;
	String prependPerm() default "";
	String baseCmd() default "";
}
