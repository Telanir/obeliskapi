package net.minegrid.obelisk.api.command;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

import net.minegrid.obelisk.api.Module;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.minegrid.obelisk.api.JavaModule;
import net.minegrid.obelisk.api.command.OCmd.Param;
import net.minegrid.obelisk.core.Foundation;
import net.minegrid.obelisk.core.agents.CommandHandler;
import net.minegrid.obelisk.core.agents.CommandHandler.CHCommand;
import net.minegrid.obelisk.core.agents.CommandHandler.CommandType;

public final class OCmdRuntime {
	boolean playerOnly = false;
	String command;
	String permission = "";
	String info = null;
	String[] paramInfo = null;
	boolean fullyRuntime = false;
	boolean syntaxExists = true;

	JavaModule module = null;
	
	public OCmdRuntime(String command) {
		this.command = command;
	}
	
	OCmdRuntime(String command, JavaModule module, String permission, String info, String[] paramInfo, boolean playerOnly) {
		this.command = command;
		this.permission = permission;
		this.info = info;
		this.paramInfo = paramInfo;
		this.playerOnly = playerOnly;
		this.fullyRuntime = true;
		this.module = module;
	}
	
	private OCmdRuntime(CHCommand c) {
		this.command = c.key;
		this.permission = c.permission;
		this.info = c.info;
		this.paramInfo = c.paramInfo;
		this.playerOnly = c.playerOnly;
		this.fullyRuntime = true;
		this.module = c.module;
	}

	public Module getModule() {
		return module;
	}
	
	public String getCommand() {
		StringBuilder cmd = new StringBuilder();
		for(String element : command.split("\\s+"))
			if(!element.startsWith("-"))
				cmd.append(" ").append(element);
		return cmd.toString().substring(1);
	}
	
	public boolean hasParameters() {
		for(String s : command.split("\\s+"))
			if(s.startsWith("-"))
				return true;
		return false;
	}
	
	public String[] getRawParameters() {
		List<String> params = new ArrayList<>();
		for(String s : command.split("\\s+")) {
			if(s.startsWith("-")) {
				params.add(s);
			}
		}
		return params.toArray(new String[0]);
	}
	
	public String[] getParameters() {
		List<String> params = new ArrayList<>();
		for(String s : command.split("\\s+")) {
			if(s.startsWith("-")) {
				ParseElement pe = Foundation.getCommandHandler().elementFrom(s);
        		if(pe == null)
        			params.add(s);
        		else
        			params.add(s.substring(0, s.indexOf(pe.getParse())));
			}
		}
		return params.toArray(new String[0]);
	}
	
	public boolean syntaxExists() {
		return syntaxExists;
	}
	
	public boolean isNonObelisk() {
		return !Foundation.getCommandHandler().cmdBaseExists(command.split("\\s+")[0]);
	}
	
	public boolean isPlayerOnly() {
		return playerOnly;
	}
	
	public String getBaseCommand() {
		return command.split("\\s+")[0].replace("/", "");
	}
	
	public String getCommandSequence() {
		return command;
	}
	
	public String getPermission() {
		return permission;
	}
	
	public String getInfo() {
		return info;
	}
	
	public String[] getParamInfo() {
		return paramInfo;
	}
	
	/*public OCmdRuntime setPlayerOnly() {
		runtimeCheck();
		this.playerOnly = true;
		return this;
	}*/
	
	public OCmdRuntime setPermission(String permission) {
		runtimeCheck();
		this.permission = permission;
		return this;
	}
	
	public OCmdRuntime setInfo(String info) {
		runtimeCheck();
		this.info = info;
		return this;
	}
	
	public OCmdRuntime setParamInfo(String[] paramInfo) {
		runtimeCheck();
		this.paramInfo = paramInfo;
		return this;
	}
	
	public void runtimeCheck() {
		if(fullyRuntime)
			throw new RuntimeException("ObeliskRuntimeCommand cannot be modified when registered.");
	}
	
	public void insert(JavaModule module, ObeliskListener listener, String tag) throws IllegalArgumentException {
		if(fullyRuntime)
			throw new RuntimeException("ObeliskRuntimeCommand already registered. (" + tag + ")");
		Class<?> clazz = listener.getClass();
        AnnotatedElement ae_clazz = (AnnotatedElement)clazz;
        OCmdMod mod = ae_clazz.getAnnotation(OCmdMod.class);
        
        for(Method m : clazz.getDeclaredMethods()) {
			try {
				if(!m.isAccessible())
					m.setAccessible(true);
				if(m.isAnnotationPresent(OCmd.class)) {
					AnnotatedElement element = (AnnotatedElement) m;
					OCmd oc = element.getAnnotation(OCmd.class);
					if(oc != null && m.getParameterCount() > 1 && oc.cmd().startsWith("@") && oc.cmd().replaceFirst("@", "").equals(tag)) {
						Parameter[] pmts = m.getParameters();
						if(pmts[0].getType() != String.class)
							return;
						boolean playerOnly = pmts[1].getType() == Player.class;
						//if(mod != null)
						//	if(mod.playerOnly())
						//		playerOnly = true;
						Class<?> sender = playerOnly ? Player.class : CommandSender.class;
						if(pmts[1].getType() == sender) {
							boolean register = false;
							String cmd = this.command;
							if(mod != null)
								if(mod.baseCmd().length() > 0) {
									if(oc.cmd().length() > 0)
										cmd = mod.baseCmd() + " " + cmd;
									else
										cmd = mod.baseCmd();
								}

							String[] args = cmd.split("\\s+");
							CommandType t = CommandType.COMMAND;

							if(args.length > 1 && pmts.length > 1) {
								boolean argFound = false;
								boolean paramFound = false;
								for(int i = 1; i < args.length; i++) {
									String s = args[i];
									if(s.startsWith("-"))
										paramFound = true;
									else
										argFound = true;
								}
								if(pmts[2].getType().isArray())
									if(argFound && pmts[2].getType().getComponentType() == String.class) {
										if(paramFound) {
											register = pmts[3].getType().isArray() && pmts[3].getType().getComponentType() == Param.class;
											t = CommandType.ARGS_AND_PARAMS;
										} else {
											register = true;
											t = CommandType.ARGS;
										}
									} else if(paramFound && pmts[2].getType().getComponentType() == Param.class) {
										register = pmts[2].getType().getComponentType() == Param.class;
										t = CommandType.PARAMS;
									}
							} else
								register = true;

							// If ALL accepting method.
							if(pmts.length == 3 && pmts[1].getType() == CommandSender.class
									&& pmts[2].getType().isArray() && pmts[1].getType().getComponentType() == String.class
									&& pmts[3].getType().isArray() && pmts[2].getType().getComponentType() == Param.class) {
								register = true;
								t = CommandType.ARGS_AND_PARAMS;
							}

							if(register) {
								String perm = this.permission;
								if(mod != null)
									if(mod.prependPerm().length() > 0)
										perm = perm.length() > 0 ? mod.prependPerm()+"."+perm : mod.prependPerm();
								CHCommand chc = new CHCommand(module, listener, m, cmd, perm, playerOnly, info, paramInfo, t);
								chc.setRuntime();
								Foundation.getCommandHandler().registerCommand(chc);
								fullyRuntime = true;
							}
						}
					}
				}
			} catch(Exception e) {}
        }
	}
}
