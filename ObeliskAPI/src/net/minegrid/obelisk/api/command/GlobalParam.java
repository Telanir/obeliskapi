package net.minegrid.obelisk.api.command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.command.CommandSender;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.command.OCmd.Param;
import net.minegrid.obelisk.core.agents.CommandHandler.CHCommand;

public final class GlobalParam {
	String param;
	String parse;
	Execution code;
	Module mod;
	
	public GlobalParam(String param, String parse, Execution code){
		this.param = "-"+param;
		this.parse = parse;
		this.code = code;
	}
	
	public GlobalParam(String param, Execution code) {
		this(param, null, code);
	}
	
	public Module getModule() {
		return mod;
	}
	
	public void setModule(Module m) {
		mod = m;
	}
	
	public String getParameter() {
		return param;
	}
	
	public String getParse() {
		return parse;
	}
	
	public boolean hasParse() {
		return parse != null;
	}
	
	void execute(Input input) {
		code.execute(input);
	}
	
	public interface Execution {
		void execute(Input input);
	}
	
	public static class Input {
		CommandSender cs;
		String[] args;
		Param[] params;
		GlobalParam p;
		String[] pval;
		OCmdRuntime command;
		
		public Input(CommandSender cs, String[] args, Param[] params, GlobalParam p, CHCommand c) {
			List<Param> ps = new ArrayList<>(Arrays.asList(params));
			command = new OCmdRuntime(c.key, c.module, c.permission, c.info, c.paramInfo, c.playerOnly);
			this.cs = cs;
			this.args = args;
			this.p = p;
			for(Param t : ps) {
				if(t.param.equals(p.param)) {
					pval = t.getValues();
					ps.remove(t);
					break;
				}
			}
			params = ps.toArray(new Param[0]);
		}
		
		Input(CommandSender cs, String[] args, Param[] params, GlobalParam p, OCmdRuntime c) {
			List<Param> ps = new ArrayList<>(Arrays.asList(params));
			command = c;
			this.cs = cs;
			this.args = args;
			this.p = p;
			for(Param t : ps) {
				if(t.param.equals(p.param)) {
					pval = t.getValues();
					ps.remove(t);
					break;
				}
			}
			params = ps.toArray(new Param[0]);
		}
		
		public String getRawInput() {
			return command.getBaseCommand() + " " + StringUtils.join(args, ' ');
		}
		
		public String getInputExcludingGlobalParam() {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < args.length; i++)
				if(args[i].equals(p.param)) {
					i++;
					if(p.hasParse())
						i++;
				} else
					sb.append(args[i]).append(" ");
			return command.getBaseCommand() + " " + sb.toString().trim();
		}
		
		public String getInputExcludingParams() {
			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < args.length; i++){
				if(params != null)
					for(Param p : params) {
						if(args[i].equals(p.param)) {
							if(p.hasValues())
								for(String s : p.getValues())
									i += s.split("\\s+").length;
						}
					}
				if(i < args.length && args[i].equals(p.param)) {
					i++;
					if(p.hasParse())
						i++;
				} 
				if(i < args.length)
					sb.append(args[i]).append(" ");
			}
			return command.getBaseCommand() + " " + sb.toString().trim();
		}
		
		public CommandSender getSender() {
			return cs;
		}
		
		public String[] getArguments() {
			return args;
		}
		
		public Param[] getParameters() {
			return params;
		}
		
		public GlobalParam getGlobalParameter() {
			return p;
		}
		
		public String[] getGlobalParameterEntries() {
			return pval;
		}
		
		public OCmdRuntime getCommand() {
			return command;
		}
	}
}
