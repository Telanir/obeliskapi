package net.minegrid.obelisk.api.command;

import org.bukkit.command.CommandSender;

public interface ParseElement {
	ParseAction getAction();
	String getName();
	String getParse();
	String getErrorMessage();
	void sendErrorMessage(CommandSender sender, String value);
	
	public interface ParseAction {
		String parse(String argument);
	}
}
