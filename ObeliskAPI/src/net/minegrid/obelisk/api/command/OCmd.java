package net.minegrid.obelisk.api.command;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface OCmd {
	//boolean playerOnly() default false;
	String cmd();
	String perm() default "";
	String info() default "No help provided.";
	String[] paramInfo() default {};
	
	public static class Param {
		String param;
		List<String> values;
		
		public Param(String p){
			param = p;
		}
		
		public String getParameter() {
			return param;
		}
		
		public void addValue(String p) {
			if(values == null)
				values = new ArrayList<>();
			values.add(p);
		}
		
		public String[] getValues() {
			if(values == null)
				return new String[0];
			return values.toArray(new String[0]);
		}
		
		public String getFirstValue() {
			String[] vals = getValues();
			return vals.length == 0 ? null : vals[0];
		}
		
		public boolean hasValues() {
			return values != null;
		}
		
		@Override
		public String toString() {
			return "{" + param + ": " + (hasValues() ? Arrays.deepToString(values.toArray(new String[0])) : "null") + "}";
		}
	}
}
