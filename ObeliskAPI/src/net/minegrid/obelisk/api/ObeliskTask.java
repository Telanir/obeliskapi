package net.minegrid.obelisk.api;

/**
 * The {@code ObeliskTask} is responsible for running a repeating task for a specific {@code Module}.
 * These tasks can be enabled and disabled, and their interval can be changed during runtime.
 * 
 * @author Maxim Chipeev
 * @version 1.0
 * @since 1.0.0
 */
public interface ObeliskTask {
	/**
	 * Assigns a new interval for operation.
	 * @param interval value in ticks
	 */
	public void setInterval(long interval);
	/**
	 * Enables or disables the {@code ObeliskTask}.
	 * @param enabled if true, executes task with interval
	 */
	public void setEnabled(boolean enabled);
	/**
	 * Returns whether or not the {@code ObeliskTask} is
	 * currently being processed.
	 * @return false if not being executed.
	 */
	public boolean isEnabled();
	/**
	 * Returns the owning {@code Module} for this task.
	 * @return null if there is no {@code Module} responsible.
	 */
	public Module getModule();
}
