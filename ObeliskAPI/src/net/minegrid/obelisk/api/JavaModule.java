package net.minegrid.obelisk.api;

import java.io.File;
import java.io.PrintStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.craftbukkit.v1_8_R3.Overridden;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import net.minegrid.obelisk.core.Foundation;
import net.minegrid.obelisk.core.agents.CommandHandler;
import net.minegrid.obelisk.core.agents.CoreFileHandler;
import net.minegrid.obelisk.api.command.GlobalParam;
import net.minegrid.obelisk.api.command.ObeliskListener;

/**
 * JavaModule is the architecture responsible for
 * delivering tangible content to end users.
 * 
 * @author Maxim Chipeev
 * @version 1.0
 * @since 1.0.0
 */
@SuppressWarnings("unused")
public abstract class JavaModule implements Module, Listener {
    private transient CoreInfo info;
    //private transient ModuleSessionLog sessionLog;
    private transient boolean enabled = false;
    private transient DataClump data;

    private transient Monitor.Rule debug;
    private transient Monitor.Rule general;
    private transient MonitorSender.File debugFile;
    private transient MonitorSender.File generalFile;

    private boolean debugging;

    public CoreInfo initInfo() {
    	return null;
    }
    
    protected abstract void onEnable();
    protected abstract void onDisable();

    public JavaModule() {
        if(!getModuleFolder().exists())
            getModuleFolder().mkdirs();
    }

    private CoreInfo getModuleInfo() {
        if(info == null) {
        	info = CoreInfo.fromClass(getClass());
        	if(info == null)
        		info = initInfo();
        }
        return info;
    }

    @Override
    public MonitorSender.File getMonitorFileSender() {
        return generalFile;
    }

    @Override
    public MonitorSender.File getDebugFileSender() {
        return debugFile;
    }

    @Override
    public final void setEnabled(boolean on) {
        if(on != enabled) {
            enabled = on;
            Foundation.getDaemon().enable(this);
        }
    }
    
	private void enable() {
        long start = System.currentTimeMillis();
        try {
            onEnable();
            startupLogSession();
            enabled = true;
            Bukkit.getPluginManager().registerEvents(this, Foundation.getInstance());
            Foundation.sendTrivialMessage(getDisplayName() + " enabled successfully! (" + (System.currentTimeMillis() - start) + "ms)");
        } catch(Exception e) {
            Foundation.sendErrorMessage(getDisplayName() + " has failed to enable!");
            e.printStackTrace(Foundation.getFileSender().getStream());
            e.printStackTrace();
        }
    }

    private void disable() {
        long start = System.currentTimeMillis();
        try {
            enabled = false;
            endLogSession();
            onDisable();
            HandlerList.unregisterAll(this);
            Foundation.sendTrivialMessage(getDisplayName() + " has been successfully disabled. (" + (System.currentTimeMillis() - start) + "ms)");
        } catch(Exception e) {
            Foundation.sendErrorMessage(getDisplayName() + " has failed to disable safely!");
            e.printStackTrace(Foundation.getFileSender().getStream());
            e.printStackTrace();
        }
    }

    @Override
    public boolean isDebugging() {
        return debugging;
    }

    @Override
    public void setDebugging(boolean debugging) {
        this.debugging = debugging;
    }

    @Override
    public Monitor.Rule getDebug() {
        return debug;
    }

    @Override
    public Monitor.Rule getMonitor() {
        return general;
    }

    @Override
    public void debug(String message) {
        debug(Monitor.TRIVIAL, message);
    }

    @Override
    public void debug(Monitor level, String message) {
        if(debugging)
            debug.pass(level, message);
    }

    @Override
    public void severe(String message) {
        general.pass(Monitor.SEVERE, message);
    }

    @Override
    public void minor(String message) {
        general.pass(Monitor.MINOR, message);
    }

    @Override
    public void trivial(String message) {
        general.pass(Monitor.TRIVIAL, message);
    }

    private void startupLogSession() {
        String file_name = LocalDate.now()+"@"+ LocalTime.now().truncatedTo(ChronoUnit.SECONDS);
        File debug_file = new File(getModuleFolder()+File.separator+file_name+".debug.log");
        File general_file = new File(getModuleFolder()+File.separator+file_name+".log");

        MonitorSender.Single toDebugConsole = MonitorSender.Single.build(Monitor.TRIVIAL, Bukkit.getConsoleSender());
        debugFile = MonitorSender.File.build(Monitor.TRIVIAL, debug_file);
        generalFile = MonitorSender.File.build(Monitor.MINOR, general_file);

        debug = Monitor.buildRule().timeParser().moduleParser(this)
                .sender(debugFile).sender(toDebugConsole).create();
        general = Monitor.buildRule().timeParser().moduleParser(this)
                .sender(generalFile).singleSender(Monitor.MINOR, Bukkit.getConsoleSender())
                .sender(toDebugConsole).sender(debugFile).create();

        //sessionLog = new ModuleSessionLog(ModuleSessionLog.LoggerState.ACTIVE_ERROR_ONLY);
    }

    private void endLogSession() {
        debug.terminate();
        general.terminate();
        generalFile = null;
        debugFile = null;
        //sessionLog.endSession();
        //FileManager.save(new File(Module.getDataFolder() + File.separator + "Logs" + File.separator + sessionLog.getDateString() + ".yml"), SaveMethod.YAML, sessionLog);
    }

    protected final void registerEvents() {
        unregisterEvents();
        getFoundation().getServer().getPluginManager().registerEvents(this, getFoundation());
    }

    protected final void unregisterEvents() {
        HandlerList.unregisterAll(this);
    }

    static Foundation getFoundation() {
        return Foundation.getInstance();
    }
    
    final public void registerCommands(ObeliskListener listener) {
    	Obelisk.registerCommands(listener, this);
    }
    
	final public void unregisterCommands() {
		CommandHandler.getCommand().unregister(this);
	}
	
    final public GlobalParam registerGlobalParameter(String tag, GlobalParam.Execution code) {
    	return Obelisk.registerGlobalParameter(this, tag, code);
    }
	
    final public GlobalParam registerGlobalParameter(String tag, String parse, GlobalParam.Execution code) {
    	return Obelisk.registerGlobalParameter(this, tag, parse, code);
    }
    
    final public ObeliskTask registerTask(String tag, long interval, Runnable run) {
    	return Obelisk.registerTask(this, tag, interval, run);
    }
    
    /**
     * Returns the control-plugin <code>Foundation</code> that manages all modules.
     * @return <code>Foundation</code>
     */
    public final Plugin getPlugin() {
    	return Foundation.getInstance();
    }

    public final Server getServer() {
        return getFoundation().getServer();
    }

    public final File getDataFolder() {
        return getFoundation().getDataFolder();
    }

    @Override
    public final File getModuleFolder() {
        return new File(getDataFolder()+File.separator+getName().toLowerCase()+File.separator);
    }

    public final boolean isEnabled() {
        return enabled;
    }

    public final String getName() {
        return getModuleInfo().name;
    }

    public final String getDisplayName() {
        return getModuleInfo().displayName;
    }

    public final String getFileName() {
        return getModuleInfo().fileName;
    }

    public final ModuleType getType() {
    	return getModuleInfo().type;
    }

    public final String getVersion() {
        return getModuleInfo().version;
    }
    
	public final List<Class<? extends Module>> getDependencies() { return new ArrayList<>(getModuleInfo().dependencies); }

    public final String getDescription() {
        return getModuleInfo().info;
    }

    public final ChatColor getColor() {
        return getModuleInfo().color;
    }

    public final Class<? extends DataClump> getDataClumpClass() {
        return getModuleInfo().dataClass;
    }
    
    @SuppressWarnings("unchecked")
	public final <T extends DataClump> T getCoreData(Class<T> t) {
    	return (T) getDataClumpClass().cast(getCoreData());
    }
    
    /**
     * Returns the data-class associated with this <code>JavaModule</code>.
     * @return null if there was no class specified.
     */
    public final DataClump getCoreData() {
    	if(getDataClumpClass() == null)
    		return null;
        if(data == null) {
            data = (DataClump) CoreFileHandler.load(new File(getDataFolder() + File.separator + getFileName()));
            if(data == null) {
                try {
                    data = getDataClumpClass().newInstance();
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return data;
    }

    public final FileManager.SaveMethod getSaveMethod() {
    	if(getDataClumpClass() == null)
    		return FileManager.SaveMethod.XML;
        return data.getSaveMethod();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof JavaModule) {
            JavaModule mod = (JavaModule)obj;
            return mod.getName().equals(getName());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}