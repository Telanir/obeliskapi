package net.minegrid.obelisk.api;

import java.io.File;
import java.util.concurrent.Future;

import net.minegrid.obelisk.core.Foundation;
import net.minegrid.obelisk.core.agents.CoreFileHandler;

/**
 * The <code>FileManager</code> is responsible for the processing of all File-related tasks
 * for <code>Obelisk</code> and relevant modules. 
 * Any file-related activity in <code>Obelisk</code> should pass through this class.
 * 
 * @version 1.0
 * @since 1.0.0
 * @author Maxim Chipeev
 *
 */
public interface FileManager {
	/**
	 * Asynchronously saves an object to the specified <code>File</code> with the given method.
	 * Save is not guaranteed to be successful.
	 * 
	 * @param file File starting from the Obelisk directory.
	 * @param method Method used to save the file.
	 * @param o Object to save.
	 */
	static void saveAsync(File file, SaveMethod method, Object o) {
		CoreFileHandler.saveAsync(file, method, o);
	}
	
	/**
	 * Asynchronously saves an object to the specified <code>File</code> with the given method.
	 * The <code>File</code> is automatically created with Obelisk's filepath
	 * as the root directory. If <code>folder</code> is not null then <code>filename</code>
	 * is appended to the folder.
	 * 
	 * Save is not guaranteed to be successful.
	 * 
	 * @param folder File starting from the Obelisk directory.
	 * @param filename Name of the file to append.
	 * @param method Method used to save the file.
	 * @param o Object to save.
	 */
	static void saveAsync(String folder, String filename, SaveMethod method, Object o) {
		CoreFileHandler.saveAsync(folder, filename, method, o);
	}

	/**
	 * Synchronously saves an object with the given method.
	 * The <code>File</code> is automatically created with Obelisk's filepath
	 * as the root directory. If <code>folder</code> is not null then <code>filename</code>
	 * is appended to the folder.
	 * 
	 * The directory will be automatically created as necessary.
	 * 
	 * @param folder Can be null, designates a folder to place the file in.
	 * @param filename Designates the file-name for the save.
	 * @param method Method used to save the file.
	 * @param o Object to be saved.
	 * @return Returns false if the file could not be saved.
	 */
	static boolean save(String folder, String filename, SaveMethod method, Object o) {
		return CoreFileHandler.save(folder, filename, method, o);
	}
	
	/**
	 * Synchronously saves an object to the specified <code>File</code> with the given method.
	 * 
	 * @param file File starting from the Obelisk directory.
	 * @param method Method used to save the file.
	 * @param o Object to save.
	 * @return Returns false if the file could not be saved.
	 */
	public static boolean save(File file, SaveMethod method, Object o) {
		return CoreFileHandler.save(file, method, o);
	}

	/**
	 * Returns true if the file could be found.
	 * The <code>File</code> used to verify existence of the path
	 * is automatically created with Obelisk's filepath
	 * as the root directory. 
	 * 
	 * If <code>folder</code> is not null then <code>filename</code>
	 * is appended to the folder.
	 * 
	 * @param folder
	 * @param filename
	 * @return
	 */
	static boolean fileExists(String folder, String filename) {
		return CoreFileHandler.fileExists(folder, filename);
	}

	/**
	 * Synchronously retrieves the object with the specified filepath.
	 * The <code>File</code> is automatically created with Obelisk's filepath
	 * as the root directory. If <code>folder</code> is not null then <code>filename</code>
	 * is appended to the folder.
	 * 
	 * @param folder Can be null, designates the folder the file is in.
	 * @param filename Denotes the name of the file.
	 * @return null if the file does not exist or failed to load.
	 */
	static Object load(String folder, String filename) {
		return CoreFileHandler.load(folder, filename);
	}

	/**
	 * Synchronously retrieves the object at the specified <code>File</code>.
	 * 
	 * <code>SQL</code> will return null automatically.
	 * <code>CUSTOM</code> will throw an exception as it is defined as an exclusion from FileHandler's domain.
	 * 
	 * @param file From where to retrieve the object.
	 * @return null if the file does not exist or failed to load.
	 */
	static Object load(File file) {
		return CoreFileHandler.load(file);
	}

	/**
	 * Asynchronously retrieves the object at the specified <code>File</code>.
	 * 
	 * @param file From where to retrieve the object.
	 * @return Future<?> object
	 */
	public static Future<?> loadAsync(File file) {
		return CoreFileHandler.loadAsync(file);
	}
	
	/**
	 * Asynchronously retrieves the object with the specified filepath.
	 * The <code>File</code> is automatically created with Obelisk's filepath
	 * as the root directory. If <code>folder</code> is not null then <code>filename</code>
	 * is appended to the folder.
	 * 
	 * @param folder Can be null, designates the folder the file is in.
	 * @param filename Denotes the name of the file.
	 * @return Future<?> object
	 */
	public static Future<?> loadAsync(String folder, String filename) {
		return CoreFileHandler.loadAsync(folder, filename);
	}

	/**
	 * <code>SaveMethod</code> designates a save-method to use when saving
	 * or finds the method applied on an existing file.
	 * 
	 * <code>SQL</code> is not a currently supported method,
	 * use <code>CUSTOM</code> instead.
	 * 
	 * @version 1.0
	 * @since 1.0.0
	 * @author Maxim
	 *
	 */
	public enum SaveMethod {
		XML, 
		YAML, 
		SQL, 
		CUSTOM;
		
		/**
		 * Returns the likely <code>SaveMethod</code> used to save
		 * a specific file.
		 * 
		 * .xml will return <code>XML</code>
		 * .yml will return <code>YML</code>
		 * If there is no extension, defaults to XML.
		 * 
		 * @param f File to examine.
		 * @return Returns null if the extension is unexpected.
		 */
		public static SaveMethod getFileType(File f) {
			String[] name = f.getName().split("\\.");
			if(name.length > 1) {
				String suffix = name[name.length - 1];
				if(suffix == null) {
					return XML;
				} else {
					switch(suffix) {
					case "xml":
						return XML;
					case "yml":
						return YAML;
					default:
						return null;
					}
				}
			}
			return XML; // defaults to XML
		}

		/**
		 * Returns the extension for the specified file-type.
		 * @return empty-string for <code>SQL</code> and <code>CUSTOM</code>.
		 */
		public String getExtension() {
			switch(this) {
			case YAML:
				return ".yml";
			case XML:
				return ".xml";
			default:
				return "";
			}
		}
	}
}
