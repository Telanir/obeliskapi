package net.minegrid.obelisk.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.minegrid.obelisk.api.tools.UUIDTool;
import net.minegrid.obelisk.api.tools.UserTool;

/**
 * The User file is responsible for holding all data relating to a specific player.
 * Variables are registered on the fly and saved automatically. Temporary variables
 * are specific to a single User session.
 * 
 * Temporary variables are not guaranteed to last through an entire {@code Obelisk} session if
 * the user disconnects. These variables are not cleared on a user restarting their connection.
 * 
 * @author Maxim
 * @version 1.0
 * @since 1.0.0
 */
public final class User {
	Map<String, UserVar<?>> vars;
	transient Map<String, UserVar<?>> temp_vars;
	UUID uniqueId;
	String name;
	transient Player p;
	
	private User() {
		vars = new ConcurrentHashMap<>();
		temp_vars = new ConcurrentHashMap<>();
	}
	
	private User(OfflinePlayer p) {
		if(p.isOnline()) {
			Player on = p.getPlayer();
			uniqueId = on.getUniqueId();
			name = on.getName();
		} else if(p.hasPlayedBefore()) {
			uniqueId = p.getUniqueId();
			name = p.getName();
		} else {
			try {
				name = p.getName();
				uniqueId = UUIDTool.getUUIDOf(p.getName());
			} catch(Exception e) {
				name = null;
				uniqueId = null;
				return;
			}
		}
		vars = new ConcurrentHashMap<>();
		temp_vars = new ConcurrentHashMap<>();
	}
	
	public void checkSafety() {
		if(vars == null)
			vars = new ConcurrentHashMap<>();
		if(temp_vars == null)
			temp_vars = new ConcurrentHashMap<>();
	}
	
	/**
	 * Returns whether or not the the user has the permission.
	 * @param permission perm to check
	 * @return automatically false if {@code Player} is offline
	 */
	public boolean hasPermission(String permission) {
		if(!isOnline())
			return false;
		return UserTool.hasPerm(toCommandSender(), permission);
	}
	
	/**
	 * Returns the associated player.
	 * @return null if the {@code Player} is offline.
	 */
	public Player getPlayer() {
		if(p != null && p.isOnline())
			return p;
		else
			p = Bukkit.getPlayer(uniqueId);
		return p;
	}
	
	/**
	 * Returns the status of the player.
	 * @return false if offline.
	 */
	public boolean isOnline() {
		return Bukkit.getPlayer(uniqueId) != null;
	}
	
	/**
	 * Returns the UUID for this User file.
	 * @return UUID of player.
	 */
	public UUID getUniqueId() {
		return uniqueId;
	}
	
	/**
	 * Returns the last known name of the User.
	 * @return Last known name.
	 */
	public String getLastKnownName() {
		return name;
	}
	
	/**
	 * Returns an immutable array of all variable keys.
	 * @return Temporary & permanent variables.
	 */
	public String[] varKeys() {
		List<String> var = new ArrayList<>(vars.keySet());
		var.addAll(temp_vars.keySet());
		return var.toArray(new String[0]);
	}
	
	/**
	 * Assigns a variable, overriding any previous values.
	 * If variable was previously temporary, this makes it permanent.
	 * @param mod Owning {@code Module}.
	 * @param name Key
	 * @param var Value
	 */
	public <E> void setVar(Module mod, String name, E var) {
		setVar(name, new UserVar<E>(mod, var));
	}
	
	/**
	 * Assigns a variable, does not override.
	 * If variable was previously temporary, this makes it permanent.
	 * @param mod Owning {@code Module}.
	 * @param name Key
	 * @param var Value
	 */
	public <E> void addVar(Module mod, String name, E var) {
		addVar(name, new UserVar<E>(mod, var));
	}

	public void setVar(String name, UserVar<?> var) {
		temp_vars.remove(var.getTag(name));
		vars.put(var.getTag(name), var);
	}
	
	public void addVar(String name, UserVar<?> var) {
		temp_vars.remove(var.getTag(name));
		vars.putIfAbsent(var.getTag(name), var);
	}
	
	public void setTempVar(String name, UserVar<?> var) {
		temp_vars.put(var.getTag(name), var);
	}
	
	public void addTempVar(String name, UserVar<?> var) {
		temp_vars.putIfAbsent(var.getTag(name), var);
	}
	
	/**
	 * Assigns a temporary variable, overriding any previous values.
	 * This variable does not persist across {@code Server} sessions.
	 * @param mod Owning {@code Module}.
	 * @param name Key
	 * @param var Value
	 */
	public <E> void setTempVar(Module mod, String name, E var) {
		setTempVar(name, new UserVar<E>(mod, var));
	}
	
	/**
	 * Assigns a temporary variable, does not override.
	 * This variable does not persist across {@code Server} sessions.
	 * @param mod Owning {@code Module}.
	 * @param name Key
	 * @param var Value
	 */
	public <E> void addTempVar(Module mod, String name, E var) {
		addTempVar(name, new UserVar<E>(mod, var));
	}
	
	/**
	 * Deletes a variable for the specified key.
	 * @param name Key.
	 */
	public void removeVar(Module m, String name) {
		String tag = m == null ? name : m.getName() + "."  + name;
		vars.remove(tag);
		temp_vars.remove(tag);
	}
	
	/**
	 * Returns whether or not the variable persists.
	 * @param name Key.
	 * @return true if the variable does not persist.
	 */
	public boolean isTemporary(Module m, String name) {
		return temp_vars.containsKey(m == null ? name : m.getName() + "." + name);
	}

	/**
	 * Converts a temporary variable into a permanent one.
	 * @param m Owning {@code Module}
	 * @param name Key
     */
	public void persist(Module m, String name) {
		if(varExists(m, name))
			setVar(m, name, getVar(m, name));
	}

	/**
	 * Returns whether or not the variable exists.
	 * @param name Key.
	 * @return true if the variable is contained in the User file.
	 */
	public boolean varExists(Module m, String name) {
		return isTemporary(m, name) || vars.containsKey(m == null ? name : m.getName() + "." + name);
	}
	
	/**
	 * Returns the {@code Player} cast to a {@code CommandSender}.
	 * @return null if the {@code Player} is not online.
	 */
	public CommandSender toCommandSender() {
		return Bukkit.getPlayer(uniqueId);
	}
	
	public static User getUser(Player p) {
		return Obelisk.getUser(p);
	}
	
	public static User getUser(UUID id) {
		return Obelisk.getUser(id);
	}
	
	/**
	 * Returns the value associated to a key.
	 * @param name Key.
	 * @return null if there is no temporary or permanent value associated.
	 */
	@SuppressWarnings("unchecked")
	public <T> T getVar(Module m, String name) {
		String tag = m == null ? name : m.getName() + "." + name;
		if(temp_vars.containsKey(tag))
			return (T) temp_vars.get(tag).get();
		return (T) vars.get(tag).get();
	}
	
	public static final class UserVar<E> {
		String module = null;
		E var;
		
		public UserVar(Module mod, E var) {
			if(mod != null)
				module = mod.getName();
			this.var = var;
		}
		
		String getTag(String tag) {
			return module == null ? tag : module + "." + tag;
		}
		
		public E get() {
			return var;
		}
	}
}
