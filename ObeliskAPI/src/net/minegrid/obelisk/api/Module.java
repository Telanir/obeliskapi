package net.minegrid.obelisk.api;

import java.io.File;
import java.lang.reflect.AnnotatedElement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import net.minegrid.obelisk.api.builders.InfoBuilder;
import net.minegrid.obelisk.core.Foundation;

import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.plugin.Plugin;

/**
 * Provides content in one of three ways:
 * <ul>
 * <li>Feature - adds user-targeted content, typically small</li>
 * <li>Module - provides some architecture for new content and delivers end-user material</li>
 * <li>Library - provides architecture for new content</li>
 * </ul>
 * The <code>Module</code> delivers content built on the <code>Foundation</code> framework.
 * 
 * @author Maxim Chipeev
 * @version 1.0
 * @since 1.0.0
 */
public interface Module extends SimpleColors {
	boolean isEnabled();
    void setEnabled(boolean b);
    
    /**
     * The save interval is defined in ticks, 20 per one second.
     * Modules are automatically saved each time this defined interval
     * is passed in the system time.
     * 
     * @return Interval in ticks.
     */
    default long tickSaveInterval() {
        return 12000L;
    }

    String getName();
    String getDisplayName();
    String getFileName();
    String getVersion();
    String getDescription();
    List<Class<? extends Module>> getDependencies();
    ChatColor getColor();
    ModuleType getType();
    File getModuleFolder();

    Monitor.Rule getMonitor();
    Monitor.Rule getDebug();

    boolean isDebugging();
    void setDebugging(boolean debug);

    MonitorSender.File getMonitorFileSender();
    MonitorSender.File getDebugFileSender();

    void debug(String message);
    void debug(Monitor level, String message);

    void severe(String message);
    void minor(String message);
    void trivial(String message);

    default void attempt(Runnable r) {
        try {
            r.run();
        } catch(Exception e) {
            e.printStackTrace();
            getMonitor().severe("Attempt encountered an error! Error has been logged to file.");
            e.printStackTrace(getMonitorFileSender().getStream());
        }
    }

    /**
     * Returns whether or not the specified <code>Module</code>
     * is required in order to operate.
     * @param m
     */
    default boolean isDependent(Module m) {
        for(Class<? extends Module> clazz : getDependencies())
            if(clazz == m.getClass())
                return true;
        return false;
    }

    /**
     * Returns whether or not the specified <code>Module</code>
     * is required in order to operate.
     * @param clazz
     */
    default boolean isDependent(Class<? extends Module> clazz) {
    	return getDependencies().contains(clazz);
    }

    static Server getServer() {
        return Foundation.getServerInstance();
    }
    
    static Plugin getPlugin() {
    	return Foundation.getInstance();
    }
    
    /**
     * Returns Obelisks data-folder.
     */
    static File getDataFolder() {
        return Foundation.getInstance().getDataFolder();
    }

    class CoreInfo {
        protected String name;
        protected String fileName;
        protected String displayName;
        protected String version;
        protected String info;
        protected ChatColor color;
        protected ModuleType type;
        protected Class<? extends DataClump> dataClass;
		protected List<Class<? extends Module>> dependencies;
		
		public CoreInfo(Module m, ModuleType type, String version, String name, String desc, String fileName, String displayName, ChatColor color, Class<? extends DataClump> dataClass, List<Class<? extends Module>> dependencies) {
            this.name = name;
            this.type = type;
            this.info = desc;
            this.fileName = fileName;
            this.displayName = displayName;
            this.version = version;
            this.color = color;
            this.dataClass = dataClass;
            //this.sm = saveMethod;
            this.dependencies = dependencies;
            if(dependencies == null)
            	this.dependencies = new ArrayList<>();
        }
		
		public Class<? extends DataClump> getDataClass() {
			return dataClass;
		}
		
		public static CoreInfo fromClass(Class<? extends Module> clazz) {
            if(clazz.isAnnotationPresent(Info.class)) {
            	AnnotatedElement element = (AnnotatedElement)clazz;
            	Info ann = element.getAnnotation(Info.class);
            	InfoBuilder builder = InfoBuilder.start(ann.name());
            	builder.setColor(ann.color());
                builder.setModuleType(ann.type());
                builder.setVersion(ann.version());
                builder.setDescription(ann.info());
            	if(ann.fileName().length() > 0)
            		builder.setFileName(ann.fileName());
            	if(ann.displayName().length() > 0)
            		builder.setDisplayName(ann.displayName());
            	for(Class<? extends Module> dp : ann.dependencies())
            		builder.addDependency(dp);
            	if(ann.data() != DataClump.class)
            		builder.setDataClass(ann.data());
            	return builder.build(null);
            }
            return null;
		}
    }

    public interface CustomSave {
        void save();
        void load();
    }
    
    public abstract class DataClump {
        abstract public FileManager.SaveMethod getSaveMethod();
    }
    
    public enum StateChangeResult {
    	SUCCESS, FAILURE_ERROR, FAILURE_DEPENDENCY_NOT_MET;
    }
    
    public enum ModuleType {
    	LIBRARY,
    	MODULE,
    	FEATURE;
    }
}
