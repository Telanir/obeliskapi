package net.minegrid.obelisk.api.menu;

public interface MenuData {
	String[] varKeys();
	<E> void setVar(String name, E var);
	<E> void addVar(String name, E var);
	void removeVar(String name);
	boolean varExists(String name);
	<T> T getVar(String name);

	// FIXME create builders at later date
	/*
	public static MenuData.Builder form() {
		return new Builder();
	}

	public static MenuData.Builder form(String key, Object value) {
		return new Builder().put(key, value);
	}

	public static class Builder {
		CoreMenuData cmd;
		Builder() {
			cmd = new CoreMenuData();
		}
		
		public Builder put(String key, Object value) {
			cmd.setVar(key, value);
			return this;
		}
		
		public MenuData build() {
			return cmd;
		}
	}*/
}
