package net.minegrid.obelisk.api.menu;

import net.minegrid.obelisk.api.menu.enums.MenuAction;
import net.minegrid.obelisk.api.menu.enums.PowerTarget;

/**
 * Created by Maxim on 2016-01-28.
 */
public class PowerAction {
    MenuAction action;
    PowerTarget targetType;
    Object target;

    public PowerAction(MenuAction action, PowerTarget type) {
        this(action, type, null);
    }

    public PowerAction(MenuAction action, PowerTarget type, Object target) {
        this.action = action;
        this.targetType = type;
        this.target = target;
    }

    public static PowerAction from(MenuAction action, PowerTarget type, Object target) {
        return new PowerAction(action, type, target);
    }

    public static PowerAction from(MenuAction action, PowerTarget type) {
        return new PowerAction(action, type);
    }

    public <T> T getTarget(Class<T> type) {
        return type.cast(target);
    }

    public PowerTarget targetType() {
        return targetType;
    }

    public MenuAction getMenuAction() {
        return action;
    }

    public void setTarget(Object o) {
        this.target = o;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof PowerAction))
            return false;
        PowerAction pa = (PowerAction)obj;
        return pa.action == action && pa.targetType == targetType && pa.target == target;
    }

    @Override
    public int hashCode() {
        return 5*action.hashCode() + 7*targetType.hashCode() + 3*target.hashCode();
    }
}
