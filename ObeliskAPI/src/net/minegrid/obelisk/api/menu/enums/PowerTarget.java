package net.minegrid.obelisk.api.menu.enums;

public enum PowerTarget {
	ENTITY, BLOCK, NONE, ANY, BLOCK_OR_ENTITY;
	
	public boolean equates(PowerTarget target) {
		switch(this) {
			case BLOCK_OR_ENTITY:
				return target == BLOCK || target == ENTITY;
			case ANY:
				return true;
			default:
				return this == target;
		}
	}
}
