package net.minegrid.obelisk.api.menu.enums;

public enum SessionState {
	CLOSED,
	OPEN,
	INPUT,
	POWER,
	TRANSIT;
}
