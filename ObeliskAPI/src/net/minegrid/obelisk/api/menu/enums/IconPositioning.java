package net.minegrid.obelisk.api.menu.enums;

/**
 * Created by Maxim on 2016-01-12.
 */
public enum IconPositioning {
    APPEND, OVERRIDE;
}
