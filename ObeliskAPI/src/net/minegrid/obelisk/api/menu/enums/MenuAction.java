package net.minegrid.obelisk.api.menu.enums;

import org.bukkit.event.inventory.ClickType;

public enum MenuAction {
	LEFT, RIGHT, SHIFT_RIGHT, SHIFT_LEFT, SHIFT, NON_SHIFT, ANY, KEY;

	public boolean isShift() {
		switch(this){
			case SHIFT_RIGHT:
			case SHIFT_LEFT:
			case ANY:
			case SHIFT:
				return true;
			default:
				return false;
		}
	}

	public boolean equates(MenuAction action) {
		switch(this) {
			case SHIFT:
				return action == SHIFT_LEFT || action == SHIFT_RIGHT;
			case NON_SHIFT:
				return action == RIGHT || action == LEFT;
			case ANY:
				return true;
			default:
				return this == action;
		}
	}
	
	public static MenuAction fromClick(ClickType type) {
		switch(type) {
			case RIGHT:
				return RIGHT;
			case LEFT:
				return LEFT;
			case SHIFT_RIGHT:
				return SHIFT_RIGHT;
			case SHIFT_LEFT:
				return SHIFT_LEFT;
			default:
				return ANY;
		}
	}
}
