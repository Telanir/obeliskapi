package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.MenuSession;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-02-13.
 */
@FunctionalInterface
public interface SessionPowerExitEvent extends SessionEvent {
    void run(MenuSession session, Player player);
}
