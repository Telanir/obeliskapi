package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.MenuSession;

/**
 * This event is called when a player leaves such that the
 * total number of players in a {@code MenuSession} is 0.
 *
 * @author Maxim
 * @version 1.0
 * @since 1.0.0
 */
@FunctionalInterface
public interface SessionEmptiedEvent extends SessionEvent {
    void run(MenuSession session);
}
