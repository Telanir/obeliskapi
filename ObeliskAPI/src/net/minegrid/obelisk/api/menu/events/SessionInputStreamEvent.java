package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.SessionInput;
import net.minegrid.obelisk.api.menu.builders.InputStream;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-02-13.
 */
@FunctionalInterface
public interface SessionInputStreamEvent {
    SessionInputEvent.Result run(InputStream.Output output, MenuSession session, Player player);
}
