package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.MenuSession;

/**
 * Created by Maxim on 2016-01-09.
 */
@FunctionalInterface
public interface NewSessionEvent extends SessionEvent {
    void run(MenuSession session);
}
