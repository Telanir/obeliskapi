package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.Icon;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.MenuView;

/**
 * Created by Maxim on 2016-02-22.
 */
public interface IconVisibleEvent {
    void run(MenuSession session, MenuView view, Icon icon);
}
