package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.PowerAction;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.Powertool;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-01-28.
 */
@FunctionalInterface
public interface PowertoolEvent {
    void run(PowerAction action, Powertool tool, MenuSession session, Player player);
}
