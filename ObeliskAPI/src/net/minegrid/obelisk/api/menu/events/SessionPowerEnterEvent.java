package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.Powerbar;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-02-13.
 */
@FunctionalInterface
public interface SessionPowerEnterEvent extends SessionEvent {
    void run(MenuSession session, Player player, Powerbar bar);
}
