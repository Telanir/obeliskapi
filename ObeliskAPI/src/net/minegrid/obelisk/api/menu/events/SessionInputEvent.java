package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.SessionInput;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-02-10.
 */
@FunctionalInterface
public interface SessionInputEvent {
    Result run(SessionInput input, MenuSession session, Player player);

    enum Result {
        REOPEN, EXIT_MENU, POWERMODE, AWAIT_INPUT;
    }
}
