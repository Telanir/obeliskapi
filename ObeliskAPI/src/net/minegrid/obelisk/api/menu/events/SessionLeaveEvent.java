package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.MenuSession;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-01-09.
 */
@FunctionalInterface
public interface SessionLeaveEvent extends SessionEvent {
    void run(MenuSession session, Player player);
}
