package net.minegrid.obelisk.api.menu.events;

import net.minegrid.obelisk.api.menu.active.Icon;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.enums.MenuAction;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-01-09.
 */
@FunctionalInterface
public interface IconClickEvent {
    void run(MenuSession session, Icon icon, Player player, MenuAction action);
}
