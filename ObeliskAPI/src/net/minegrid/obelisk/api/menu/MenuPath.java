package net.minegrid.obelisk.api.menu;

public class MenuPath {
	public final static String ROOT = "/";

	String path;
	
	private MenuPath(String path) {
		if(path == null)
			path = ROOT;
		else if(path.contains(ROOT+ROOT))
			throw new IllegalArgumentException("Path cannot contain '//', must have a name.");

		if(!path.startsWith(ROOT))
			this.path = ROOT+path;
		else
			this.path = path;
	}
	
	public String getPath() {
		return path;
	}

	public String getItem() {
		if(path.equals(ROOT) || path.isEmpty())
			return ROOT;
		else if(path.endsWith("/"))
			return path.substring(path.substring(0, path.length()-1).lastIndexOf('/'), path.length());
		else
			return path.substring(path.lastIndexOf('/'), path.length());
	}

	public String getParentFolder() {
		if(path.equalsIgnoreCase(ROOT) || path.isEmpty())
			return null;
		
		int pos = path.length() - 1;
		if(path.charAt(pos) == '/')
			pos--;
		
		while(pos >= 0 && path.charAt(pos) != '/')
			pos--;
		
		return path.substring(0, pos + 1);
	}
	
	public String getFolder() {
		if(isFolder())
			return path;
		return getParentFolder();
	}
	
	public boolean isFolder() {
		return path.endsWith("/");
	}
	
	public boolean isItem() {
		return !isFolder();
	}
	
	public MenuPath nest(String folder) {
		if(isFolder())
			path += folder + "/";
		return this;
	}
	
	public MenuPath item(String itemName) {
		if(isFolder())
			path += itemName;
		return this;
	}

	@Override
	public String toString() {
		return getPath();
	}

	public static String parent(String directory) {
		return MenuPath.from(directory).getParentFolder();
	}
	
	public static MenuPath from(String directory) {
		return new MenuPath(directory);
	}
}
