package net.minegrid.obelisk.api.menu.builders;

import org.bukkit.Material;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Maxim on 2016-01-22.
 */
public class IconBuilder {
    String name;
    MaterialData data;
    Short dur = -1;
    int amount = 1;
    List<String> lore = new ArrayList<>();

    public IconBuilder(MaterialData data, String displayName) {
        this.name = displayName;
        this.data = data;
    }

    public IconBuilder(Material mat, String displayName) {
        this(new MaterialData(mat), displayName);
    }

    public static IconBuilder form(MaterialData data, String displayName) {
        return new IconBuilder(data, displayName);
    }

    public static IconBuilder form(Material mat, String displayName) {
        return new IconBuilder(mat, displayName);
    }

    public static IconBuilder interim() {
        return new IconBuilder(Material.BARRIER, "LOADING");
    }

    public IconBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public IconBuilder setDurability(int durability) {
        this.dur = (short)durability;
        return this;
    }

    public IconBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public IconBuilder setLore(String...lore) {
        this.lore = Arrays.asList(lore);
        return this;
    }

    public ItemStack toItem() {
        ItemStack item = data.toItemStack(amount);
        if(dur != (short)-1)
            item.setDurability(dur);

        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        if(!lore.isEmpty())
            meta.setLore(lore);
        meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_UNBREAKABLE);
        item.setItemMeta(meta);

        return item;
    }
}
