package net.minegrid.obelisk.api.menu.builders;

import net.minegrid.obelisk.api.menu.MenuData;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.SessionInput;
import net.minegrid.obelisk.api.menu.events.SessionInputEvent;
import net.minegrid.obelisk.api.menu.events.SessionInputStreamEvent;
import net.minegrid.obelisk.core.menu.active.CoreMenuSession;
import org.bukkit.entity.Player;

import java.util.*;

/**
 * Created by Maxim on 2016-02-13.
 */
public class InputStream {
    long id;

    MenuSession session;
    Player player;
    private Queue<CoreMenuSession.Input> inputList;

    SessionInput.Type lastType;
    String lastMsg;

    InputStream(MenuSession session, Player player) {
        this.inputList = new LinkedList<>();
        this.session = session;
        this.id = new Random().nextLong();
        this.player = player;
    }

    public void inject(SessionInputStreamEvent event) {
        inputList.add(new CoreMenuSession.Input(lastType, (input, session, player) -> {
            getList().add(input);
            return event.run(new Output(), session, player);
        }, lastMsg));
        while(!inputList.isEmpty()) {
            CoreMenuSession.Input input = inputList.remove();
            session.queueInput(player, input.getType(), input.getMessage(), input.getEvent());
        }
    }

    Queue<SessionInput> getList() {
        MenuData data = session.getData(); String key = "stm"+id;
        if(!data.varExists(key))
            data.setVar(key, new LinkedList<>());
        return data.getVar(key);
    }

    public InputStream request(SessionInput.Type type, String msg) {
        if(lastType != null) {
            inputList.add(new CoreMenuSession.Input(lastType, (input, session, player) -> {
                getList().add(input);
                return SessionInputEvent.Result.AWAIT_INPUT;
            }, lastMsg));
        }

        lastType = type;
        lastMsg = msg;

        return this;
    }

    public static InputStream create(MenuSession session, Player player) {
        return new InputStream(session, player);
    }

    public class Output {
        Output() {}

        public SessionInput readInput() {
            return getList().poll();
        }

        public <T> T read(Class<T> input) {
            SessionInput in = getList().poll();
            return (in == null) ? null : in.getInput(input);
        }
    }
}
