package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.menu.enums.MenuAction;
import net.minegrid.obelisk.api.menu.enums.PowerTarget;
import net.minegrid.obelisk.api.menu.events.PowertoolEvent;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface Powertool extends Icon {
    void setAction(MenuAction action, PowerTarget target, PowertoolEvent event);
    boolean hasAction(MenuAction action, PowerTarget target);
    void removeAction(MenuAction action, PowerTarget target);
}
