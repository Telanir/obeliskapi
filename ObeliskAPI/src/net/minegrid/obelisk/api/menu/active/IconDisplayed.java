package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.menu.enums.MenuAction;
import net.minegrid.obelisk.api.menu.events.IconClickEvent;
import net.minegrid.obelisk.api.menu.events.IconKeyEvent;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-01-28.
 */
public interface IconDisplayed extends Icon {
    void setAction(MenuAction action, IconClickEvent event);
    void setKeyAction(IconKeyEvent event);

    void runAction(MenuSession session, Player p, MenuAction action);
    void runKeyAction(MenuSession session, Player p, int key);
}
