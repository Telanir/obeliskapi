package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.builders.IconBuilder;
import net.minegrid.obelisk.api.menu.dormant.Element;
import net.minegrid.obelisk.api.menu.dormant.ElementDisplayed;
import net.minegrid.obelisk.api.menu.dormant.PendingPowertool;
import net.minegrid.obelisk.api.menu.dormant.PowerElement;
import net.minegrid.obelisk.api.menu.enums.MenuAction;
import net.minegrid.obelisk.api.menu.enums.PowerTarget;
import net.minegrid.obelisk.api.menu.events.IconClickEvent;
import net.minegrid.obelisk.api.menu.events.IconKeyEvent;
import net.minegrid.obelisk.api.menu.events.IconVisibleEvent;
import net.minegrid.obelisk.api.menu.events.PowertoolEvent;
import net.minegrid.obelisk.api.tools.UserTool;
import net.minegrid.obelisk.core.menu.dormant.CoreElement;
import net.minegrid.obelisk.core.menu.dormant.CorePendingPowertool;
import net.minegrid.obelisk.core.menu.dormant.CorePowerElement;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

import java.util.List;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface Icon {
    MenuSession getSession();
    MenuView getView();

    Module getModule();

    int getPosition();
    boolean setPosition(int pos);

    ItemStack getItem();
    void setItem(ItemStack icon);

    String getPermission();
    boolean hasAccess(Player p);

    boolean hasVisibleEvent();
    void runVisibleEvent();

    Element getParent();

    default ItemBuilder changeLore(String...lore) {
        return new ItemBuilder(this).changeLore(lore);
    }

    default ItemBuilder changeMaterial(Material material) {
        return changeMaterial(new MaterialData(material));
    }

    default ItemBuilder changeMaterial(MaterialData data) {
        return new ItemBuilder(this, data, getItem().getItemMeta().getDisplayName());
    }

    default ItemBuilder changeName(String name) {
        return new ItemBuilder(this, getItem().getData(), name);
    }

    default ItemBuilder changeItem(Material material, String name) {
        return changeItem(new MaterialData(material), name);
    }

    default ItemBuilder changeItem(MaterialData data, String name) {
        return new ItemBuilder(this, data, name);
    }

    class ItemBuilder {
        IconBuilder builder;
        Icon icon;
        private ItemBuilder(Icon icon) {
            this(icon, icon.getItem().getData(), icon.getItem().getItemMeta().getDisplayName());
        }

        private ItemBuilder(Icon icon, MaterialData data, String name) {
            this.icon = icon;
            ItemStack item = icon.getItem();
            builder = IconBuilder.form(data, name);
            if(item.getItemMeta().hasLore())
                builder.setLore(item.getItemMeta().getLore());
            builder.setAmount(item.getAmount());
        }

        public ItemBuilder changeLore(String...lore) {
            builder.setLore(lore);
            return this;
        }

        public ItemBuilder changeAmount(int amount) {
            builder.setAmount(amount);
            return this;
        }

        public ItemBuilder changeLore(List<String> lore) {
            builder.setLore(lore);
            return this;
        }

        public ItemBuilder changeDurability(int dur) {
            builder.setDurability(dur);
            return this;
        }

        public void apply() {
            icon.setItem(builder.toItem());
        }
    }

    static Powertool powertool(IconBuilder builder) {
        return new Powertool(builder);
    }

    class Powertool {
        PowerElement tool;

        private Powertool(IconBuilder builder) {
            tool = new CorePowerElement(builder.toItem(), null);
        }

        public Powertool setAction(MenuAction action, PowerTarget target, PowertoolEvent event) {
            tool.setAction(action, target, event);
            return this;
        }

        public Powertool setPermission(String permission) {
            tool.setPermission(permission);
            return this;
        }

        public PowerElement build() {
            return tool;
        }
    }

    static Button button(IconBuilder builder, IconClickEvent click) {
        return new Button(builder, click);
    }
    static Button button(IconClickEvent click) {
        return new Button(interim(), click);
    }

    class Button {
        ElementDisplayed element;

        private Button(IconBuilder builder, IconClickEvent click) {
            element = new CoreElement(builder.toItem(), null);
            element.setAction(MenuAction.LEFT, click);
        }

        public Button setVisibleEvent(IconVisibleEvent event) {
            element.setVisibleEvent(event);
            return this;
        }

        public Button setPermission(String permission) {
            element.setPermission(permission);
            return this;
        }

        public Button setAction(MenuAction mact, IconClickEvent click) {
            element.setAction(mact, click);
            return this;
        }

        public Button setKeyAction(IconKeyEvent event) {
            element.setKeyAction(event);
            return this;
        }

        public ElementDisplayed build() {
            return element;
        }
    }

    static Folder folder(IconBuilder builder, String nest) {
        return new Folder(builder, nest);
    }
    static Folder folder(String nest) {
        return new Folder(interim(), nest);
    }

    class Folder {
        String nest;
        ElementDisplayed element;
        String perm;

        private Folder(IconBuilder builder, String nest) {
            element = new CoreElement(builder.toItem(), null);
            this.nest = nest;
            setAction(MenuAction.LEFT, null);
        }

        public Folder setFolderPerm(String perm) {
            this.perm = perm;
            return this;
        }

        public Folder setAction(MenuAction mact, IconClickEvent click) {
            if(mact == MenuAction.LEFT) {
                element.setAction(MenuAction.LEFT, (session, icon, player, action) -> {
                    if(click != null)
                        click.run(session, icon, player, action);
                    if(perm == null || UserTool.hasPerm(player, perm))
                        session.openDirectory(session.getDirectory()+nest+"/");
                });
            } else {
                element.setAction(mact, click);
            }
            return this;
        }

        public Folder setVisibleEvent(IconVisibleEvent event) {
            element.setVisibleEvent(event);
            return this;
        }

        public Folder setKeyAction(IconKeyEvent event) {
            element.setKeyAction(event);
            return this;
        }

        public Folder setPermission(String permission) {
            element.setPermission(permission);
            return this;
        }

        public ElementDisplayed build() {
            return element;
        }
    }

    static IconBuilder icon(MaterialData data, String displayName) {
        return IconBuilder.form(data, displayName);
    }

    static IconBuilder icon(Material mat, String displayName) {
        return IconBuilder.form(mat, displayName);
    }

    static IconBuilder interim() {
        return IconBuilder.interim();
    }
}
