package net.minegrid.obelisk.api.menu.active;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface LocalIcon extends IconDisplayed {
    String getName();
}
