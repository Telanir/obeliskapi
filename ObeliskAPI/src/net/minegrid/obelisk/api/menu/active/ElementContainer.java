package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.menu.dormant.Element;

/**
 * Created by Maxim on 2016-01-22.
 */
public interface ElementContainer {
    ViewTree getTree();
    MenuSession getSession();

    Icon[] getIcons();

    Icon getIcon(int position);
    Icon getIcon(Element element);

    void removeIcon(int position);
    void removeIcon(Icon icon);

    int findOpenSlot();
    int findOpenSlot(int from);

    int lastHeldSlot();
    int minRows();

    void wipe();
}
