package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.dormant.Element;
import net.minegrid.obelisk.api.menu.dormant.PendingPowerbar;
import net.minegrid.obelisk.api.menu.events.DirectoryOpenEvent;

/**
 * Created by Maxim on 2016-01-12.
 */
public interface LocalView extends ElementContainer {
    String getDirectory();

    boolean hasPowerbar();
    Powerbar getPowerbar();
    void setPowerbar(Powerbar powerbar);
    void setPowerbar(PendingPowerbar powerbar);

    String getDisplayName();
    void setDisplayName(String name);
    boolean hasDisplayName();

    void setOpenEvent(String key, DirectoryOpenEvent event);
    void removeOpenEvent(String key);

    LocalIcon[] getIcons();

    void addIcon(Module owner, LocalIcon element);
    void addIcon(Module owner, int position, LocalIcon element);
    void setIcon(Module owner, int position, LocalIcon element);

    void addIcon(Module owner, String path, Element element);
    void addIcon(Module owner, String path, int position, Element element);
    void setIcon(Module owner, String path, int position, Element element);

    LocalIcon getIcon(int position);
    LocalIcon getIcon(Element element);
    LocalIcon getIcon(String name);
    LocalIcon getIconFromPath(String fullPath);
}
