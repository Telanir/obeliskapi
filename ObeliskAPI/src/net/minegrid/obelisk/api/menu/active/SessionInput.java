package net.minegrid.obelisk.api.menu.active;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

/**
 * Created by Maxim on 2016-02-10.
 */
public interface SessionInput {
    Type getType();
    <T> T getInput(Class<T> type);

    enum Type {
        BLOCK, CHAT(false, true), INTEGER(false, true), DECIMAL(false, true), PLAYER(true, false), ENTITY(true, false), ENTITY_NON_PLAYER(true, false), ICON_MENU;

        boolean entity;
        boolean chat;

        Type() {
            this(false, false);
        }

        Type(boolean entity, boolean chat) {
            this.entity = entity;
            this.chat = chat;
        }

        public boolean isEntity() {
            return entity;
        }

        public boolean match(Object o) {
            switch(this) {
                case PLAYER:
                    return o instanceof Player;
                case ENTITY:
                    return o instanceof Entity;
                case ENTITY_NON_PLAYER:
                    return (o instanceof Entity) && !(o instanceof Player);
                default:
                    return false;
            }
        }

        public boolean isChat() {
            return chat;
        }
    }
}
