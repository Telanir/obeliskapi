package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.dormant.Element;

/**
 * Created by Maxim on 2016-01-12.
 */
public interface MenuView extends ElementContainer {
    void addIcon(Module owner, Icon element);
    void addIcon(Module owner, int position, Icon element);
    void setIcon(Module owner, int position, Icon element);
    void addIcon(Module owner, Element element);
    void addIcon(Module owner, int position, Element element);
    void setIcon(Module owner, int position, Element element);
}
