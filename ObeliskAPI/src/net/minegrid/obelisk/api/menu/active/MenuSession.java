package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.MenuData;
import net.minegrid.obelisk.api.menu.dormant.Menu;
import net.minegrid.obelisk.api.menu.dormant.PendingPowerbar;
import net.minegrid.obelisk.api.menu.enums.SessionState;
import net.minegrid.obelisk.api.menu.events.SessionEvent;
import net.minegrid.obelisk.api.menu.events.SessionInputEvent;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.UUID;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface MenuSession {
    Menu getMenu();

    Module getModule();

    ViewTree getTree();
    MenuData getData();

    void putEvent(String tag, SessionEvent event);
    void removeEvent(String tag);

    SessionState getState(Player p);
    SessionState getState(UUID id);
    void setState(Player p, SessionState state);
    void setState(UUID id, SessionState state);

    void queueInput(Player player, SessionInput.Type type, String message, SessionInputEvent event);

    LocalIcon getIcon(String path);
    LocalIcon getIcon(String directory, int position);
    LocalIcon getIcon(int position);

    Icon getContextIcon(int position);

    LocalView getView(String dir);
    MenuView getContextView(String dir);
    MenuView getGlobalView();

    Powerbar getPowerbar(Player player);
    boolean hasPowerbar(Player player);

    void add(Player p);
    boolean isViewing(Player p);
    boolean isViewing(UUID id);

    Player[] getPlayers();
    List<Player> getPlayersImmutable();

    Player getFirstPlayer();
    boolean hasFirstPlayer();
    Player getAnyPlayer();

    void setDisplayName(String name);

    LocalView getView();

    String getDirectory();
    void openDirectory(String dir);
    void openDirectory(String dir, int toPage);
    void refreshDirectory();
    void clearDirectory(String directory);

    void setPage(int page);
    int getPage();

    void terminate();
}
