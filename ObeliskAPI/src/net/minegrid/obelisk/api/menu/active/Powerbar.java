package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.menu.dormant.PowerElement;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface Powerbar {
    MenuSession getSession();
    LocalView getView();

    void addTool(Powertool powertool);
    void setTool(int slot, Powertool powertool);

    void addTool(PowerElement powertool);
    void setTool(int slot, PowerElement powertool);

    void removeTool(Powertool tool);
    void removeTool(int slot);

    Powertool getToolAt(int slot);
    boolean hasToolAt(int slot);

    void wipe();
}
