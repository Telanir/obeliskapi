package net.minegrid.obelisk.api.menu.active;

import net.minegrid.obelisk.api.menu.dormant.PendingPowerbar;

/**
 * Created by Maxim on 2016-01-12.
 */
public interface ViewTree {
    MenuSession getSession();

    LocalView[] getViewTree();

    LocalView getDirectory(String dir);
    LocalView getRootDirectory();

    MenuView getGlobalView();
    MenuView getContextualView(String dir);
    MenuView setCustomView(String dir);
    void setCustomView(String dir, MenuView view);
    boolean isContextGlobal(String dir);

    Powerbar getPowerbar(String dir);
    boolean isDefaultPowerbar(String dir);
    boolean hasDefaultPowerbar();
    void setDefaultPowerbar(Powerbar powerbar);
    void setDefaultPowerbar(PendingPowerbar powerbar);

    void wipe();
}
