package net.minegrid.obelisk.api.menu.dormant;

/**
 * Created by Maxim on 2016-01-22.
 */
public interface PendingElementContainer {
    PendingViewTree getTree();

    PendingIcon[] getIcons();

    PendingIcon getIcon(int position);
    PendingIcon getIcon(Element element);

    void removeIcon(int position);
    void removeIcon(Element element);
    void removeIcon(PendingIcon icon);

    void wipe();
}
