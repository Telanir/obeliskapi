package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.menu.active.LocalIcon;
import net.minegrid.obelisk.api.menu.active.MenuView;

/**
 * Created by Maxim on 2016-01-11.
 */
public interface PendingLocalIcon extends PendingIcon {
    String getName();
    LocalIcon toIcon(MenuView element);
}
