package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.MenuData;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.events.NewSessionEvent;
import net.minegrid.obelisk.api.menu.events.SessionEvent;
import net.minegrid.obelisk.core.menu.MenuDaemon;
import net.minegrid.obelisk.core.menu.dormant.CoreMenu;
import org.bukkit.entity.Player;

import java.util.Collection;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface Menu {
    String getTitle();

    void setMinRows(int minRows);

    MenuSession newSession();
    MenuSession newSession(Player clearance);
    Collection<MenuSession> getActiveSessions();

    Module getModule();

    MenuData getData();

    PendingViewTree getTree();

    PendingLocalView getView(String dir);
    PendingView getContextView(String dir);
    PendingView getGlobalView();

    void putEvent(String tag, SessionEvent event);
    void removeEvent(String tag);

    static Menu create(Module module, String title) {
        return MenuDaemon.createMenu(module, title);
    }
}
