package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.enums.MenuAction;
import net.minegrid.obelisk.api.menu.events.IconClickEvent;
import net.minegrid.obelisk.api.menu.events.IconKeyEvent;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Map;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface Element {
    ItemStack getItem();

    boolean hasPermission();
    void setPermission(String permission);
    String getPermission();
    boolean hasAccess(Player p);

    IconKeyEvent getKeyEvent();
}
