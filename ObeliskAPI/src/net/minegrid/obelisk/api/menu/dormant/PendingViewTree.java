package net.minegrid.obelisk.api.menu.dormant;

/**
 * Created by Maxim on 2016-01-12.
 */
public interface PendingViewTree {
    Menu getMenu();

    PendingLocalView[] getViewTree();
    PendingLocalView getRootDirectory();

    PendingLocalView getDirectory(String dir);

    PendingView getGlobalView();
    PendingView getContextualView(String dir);
    PendingView setCustomView(String dir);
    void setCustomView(String dir, PendingView view);
    boolean isContextGlobal(String dir);

    PendingPowerbar getPowerbar(String dir);
    boolean isDefaultPowerbar(String dir);
    boolean hasDefaultPowerbar();
    void setDefaultPowerbar(PendingPowerbar powerbar);

    void wipe();
}
