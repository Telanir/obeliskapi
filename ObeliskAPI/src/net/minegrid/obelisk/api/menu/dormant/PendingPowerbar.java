package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.menu.active.LocalView;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.Powerbar;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface PendingPowerbar {
    Menu getMenu();

    Powerbar toPowerbar(LocalView view);

    PendingLocalView getView();

    void addTool(PowerElement element);
    void setTool(int slot, PowerElement element);

    void removeTool(PowerElement element);
    void removeTool(int slot);

    PowerElement getToolAt(int slot);
    boolean hasToolAt(int slot);

    void wipe();
}
