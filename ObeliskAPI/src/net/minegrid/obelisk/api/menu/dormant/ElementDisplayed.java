package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.menu.enums.MenuAction;
import net.minegrid.obelisk.api.menu.events.IconClickEvent;
import net.minegrid.obelisk.api.menu.events.IconKeyEvent;
import net.minegrid.obelisk.api.menu.events.IconVisibleEvent;

import java.util.Map;

/**
 * Created by Maxim on 2016-01-28.
 */
public interface ElementDisplayed extends Element {
    void setAction(MenuAction action, IconClickEvent event);
    void setKeyAction(IconKeyEvent event);

    Map<MenuAction, IconClickEvent> getClickEvents();

    boolean hasVisibleEvent();
    void setVisibleEvent(IconVisibleEvent event);
}
