package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.menu.active.MenuView;
import net.minegrid.obelisk.api.menu.active.Powerbar;
import net.minegrid.obelisk.api.menu.active.Powertool;

/**
 * Created by Maxim on 2016-01-28.
 */
public interface PendingPowertool extends PendingIcon {
    Powertool toIcon(Powerbar powerbar);
    PowerElement getElement();
}
