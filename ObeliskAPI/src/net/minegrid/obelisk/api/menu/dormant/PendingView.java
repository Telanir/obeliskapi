package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.enums.IconPositioning;

import java.util.Collection;

/**
 * Created by Maxim on 2016-01-12.
 */
public interface PendingView extends PendingElementContainer {
    PendingIcon addIcon(Module owner, Element element);
    PendingIcon addIcon(Module owner, int position, Element element);
    PendingIcon addIcon(Module owner, int position, IconPositioning positioning, Element element);
}
