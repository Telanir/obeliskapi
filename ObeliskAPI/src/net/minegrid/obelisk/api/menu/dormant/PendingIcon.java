package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.active.Icon;
import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.active.MenuView;
import net.minegrid.obelisk.api.menu.enums.IconPositioning;

/**
 * Created by Maxim on 2016-01-09.
 */
public interface PendingIcon {
    Icon toIcon(MenuView view);

    Module getModule();

    int getPosition();
    IconPositioning getPositioningType();
    void setPositioningType(IconPositioning positioningType);

    Element getElement();
}
