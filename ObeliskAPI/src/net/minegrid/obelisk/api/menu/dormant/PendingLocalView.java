package net.minegrid.obelisk.api.menu.dormant;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.menu.MenuPath;
import net.minegrid.obelisk.api.menu.active.Powerbar;
import net.minegrid.obelisk.api.menu.enums.IconPositioning;
import net.minegrid.obelisk.api.menu.events.DirectoryOpenEvent;

import java.util.Collection;

/**
 * Created by Maxim on 2016-01-12.
 */
public interface PendingLocalView extends PendingElementContainer {
    String getDirectory();

    String getDisplayName();
    void setDisplayName(String name);
    boolean hasDisplayName();

    boolean hasPowerbar();
    PendingPowerbar getPowerbar();
    void setPowerbar(PendingPowerbar powerbar);
    PendingPowerbar createPowerbar();

    void setOpenEvent(String key, DirectoryOpenEvent event);
    void removeOpenEvent(String key);

    PendingLocalIcon[] getIcons();

    PendingLocalIcon addIcon(Module owner, String path, Element element);
    PendingLocalIcon addIcon(Module owner, String path, int position, Element element);
    PendingLocalIcon addIcon(Module owner, String path, int position, IconPositioning positioning, Element element);

    PendingLocalIcon getIcon(int position);
    PendingLocalIcon getIcon(Element element);

    /**
     * Returns the first found Icon that ends with the exact
     * specified file name.
     * @param fileName
     * @return
     */
    PendingLocalIcon getIcon(String fileName);
    PendingLocalIcon getIconFromPath(String fullPath);
}
