package net.minegrid.obelisk.api.builders;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;

import net.minegrid.obelisk.api.Module;
import net.minegrid.obelisk.api.Module.CoreInfo;
import net.minegrid.obelisk.api.Module.ModuleType;

public final class InfoBuilder {
	Module.CoreInfo info;
	String name;
	String version;
	String fileName;
	String displayName;
	String description;
	ModuleType type;
	ChatColor color;
	Class<? extends Module.DataClump> data;
	List<Class<? extends Module>> dependencies;
	
	InfoBuilder(String name) {
		this.name = name;
		this.fileName = name;
		this.displayName = name;
		this.type = ModuleType.MODULE;
		color = ChatColor.WHITE;
		this.data = null;
		this.dependencies = new ArrayList<>();
	}

	public InfoBuilder setVersion(String version) {
		this.version = version;
		return this;
	}

	public InfoBuilder setDescription(String desc) {
		this.description = desc;
		return this;
	}
	
	public InfoBuilder setDisplayName(String display) {
		this.displayName = display;
		return this;
	}
	
	public InfoBuilder setModuleType(ModuleType type) {
		this.type = type;
		return this;
	}
	
	public InfoBuilder setFileName(String file) {
		this.fileName = file;
		return this;
	}
	
	public InfoBuilder setColor(ChatColor color) {
		this.color = color;
		return this;
	}
	
	public InfoBuilder setDataClass(Class<? extends Module.DataClump> data) {
		this.data = data;
		return this;
	}
	
	public InfoBuilder addDependency(Class<? extends Module> dependency) {
		this.dependencies.add(dependency);
		return this;
	}
	
	public static InfoBuilder start(String name) {
		return new InfoBuilder(name);
	}
	
	public CoreInfo build(Module instance) {
		return new CoreInfo(instance, type, version, name, description, fileName.equals(name) && data == null ? fileName + ".xml" : fileName, displayName, color, data, dependencies);
	}
}
