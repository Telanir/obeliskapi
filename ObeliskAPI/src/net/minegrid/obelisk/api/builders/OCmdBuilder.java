package net.minegrid.obelisk.api.builders;

import net.minegrid.obelisk.api.JavaModule;
import net.minegrid.obelisk.api.command.OCmdRuntime;
import net.minegrid.obelisk.api.command.ObeliskListener;

/**
 * Created by Maxim on 2016-02-22.
 */
public class OCmdBuilder {
    OCmdRuntime runtime;

    public OCmdBuilder(String cmd) {
        runtime = new OCmdRuntime(cmd);
    }

    public OCmdBuilder setPermission(String permission) {
        runtime.setPermission(permission);
        return this;
    }

    public OCmdBuilder setInfo(String info) {
        runtime.setInfo(info);
        return this;
    }

    public OCmdBuilder setParamInfo(String...paramInfo) {
        runtime.setParamInfo(paramInfo);
        return this;
    }

    public void apply(JavaModule module, ObeliskListener listener, String tag) {
        runtime.insert(module, listener, tag);
    }

    public static OCmdBuilder build(String cmd) {
        return new OCmdBuilder(cmd);
    }
}
