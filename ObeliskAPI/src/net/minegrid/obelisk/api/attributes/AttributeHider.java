package net.minegrid.obelisk.api.attributes;

import java.lang.reflect.*;

import org.bukkit.plugin.*;

import com.comphenix.protocol.*;

import org.bukkit.*;
import org.bukkit.inventory.ItemStack;

import java.util.logging.*;

import com.comphenix.protocol.reflect.*;
import com.comphenix.protocol.events.*;
import com.comphenix.protocol.events.PacketListener;

import org.bukkit.craftbukkit.v1_8_R3.inventory.*;

import java.util.*;

import net.minecraft.server.v1_8_R3.*;

/**
 * 
 * @author Pepijn Bakker
 *
 */
public class AttributeHider
{
    private static Field cC;
    
    public static void listen(final Plugin plugin) {
        try {
            (setcC(EntityPlayer.class.getDeclaredField("containerCounter"))).setAccessible(true);
        }
        catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        }
        catch (SecurityException e2) {
            throw new RuntimeException(e2);
        }
        final Set<PacketType> packets = new HashSet<PacketType>();
        packets.add(PacketType.Play.Server.SET_SLOT);
        packets.add(PacketType.Play.Server.WINDOW_ITEMS);
        ProtocolLibrary.getProtocolManager().addPacketListener((PacketListener)new PacketAdapter(plugin, packets) {
            public void onPacketSending(final PacketEvent event) {
                if (event.getPlayer().getGameMode() == GameMode.CREATIVE) {
                    return;
                }
                final PacketContainer packet = event.getPacket();
                final PacketType type = packet.getType();
                if (type == PacketType.Play.Server.WINDOW_ITEMS) {
                    try {
                        final ItemStack[] read = (ItemStack[])packet.getItemArrayModifier().read(0);
                        for (int i = 0; i < read.length; ++i) {
                            read[i] = AttributeHider.removeCustomAttribute(read[i]);
                        }
                        packet.getItemArrayModifier().write(0, (ItemStack[])read);
                    }
                    catch (FieldAccessException e) {
                        Logger.getLogger(AttributeHider.class.getName()).log(Level.SEVERE, null, (Throwable)e);
                    }
                }
                else {
                    try {
                        packet.getItemModifier().write(0, (ItemStack)AttributeHider.removeCustomAttribute((ItemStack)packet.getItemModifier().read(0)));
                    }
                    catch (FieldAccessException e) {
                        Logger.getLogger(AttributeHider.class.getName()).log(Level.SEVERE, null, (Throwable)e);
                    }
                }
            }
        });
    }
    
    public static ItemStack removeCustomAttribute(ItemStack i) {
        if (i == null) {
            return i;
        }
        i = i.clone();
        final net.minecraft.server.v1_8_R3.ItemStack item = CraftItemStack.asNMSCopy(i);
        if (item.getTag() != null && item.getTag().hasKeyOfType("AttributeModifiers", 9)) {
            final NBTTagList list = item.getTag().getList("AttributeModifiers", 10);
            final NBTTagList newList = new NBTTagList();
            for (int j = 0; j < list.size(); ++j) {
                final NBTTagCompound ctag = list.get(j);
                final UUID uuid = new UUID(ctag.getLong("UUIDMost"), ctag.getLong("UUIDLeast"));
                if (!Customizer.CUSTOM_UUID.equals(uuid)) {
                    newList.add((NBTBase)ctag);
                }
            }
            item.getTag().set("AttributeModifiers", (NBTBase)newList);
        }
        return (ItemStack)CraftItemStack.asCraftMirror(item);
    }
    
    public static net.minecraft.server.v1_8_R3.ItemStack removeCustomAttribute(final net.minecraft.server.v1_8_R3.ItemStack i) {
        if (i == null) {
            return i;
        }
        final net.minecraft.server.v1_8_R3.ItemStack item = i.cloneItemStack();
        if (item.getTag() != null && item.getTag().hasKeyOfType("AttributeModifiers", 9)) {
            final NBTTagList list = item.getTag().getList("AttributeModifiers", 10);
            final NBTTagList newList = new NBTTagList();
            for (int j = 0; j < list.size(); ++j) {
                final NBTTagCompound ctag = list.get(j);
                final UUID uuid = new UUID(ctag.getLong("UUIDMost"), ctag.getLong("UUIDLeast"));
                if (!Customizer.CUSTOM_UUID.equals(uuid)) {
                    newList.add((NBTBase)ctag);
                }
            }
            item.getTag().set("AttributeModifiers", (NBTBase)newList);
        }
        return item;
    }

	public static Field getcC() {
		return cC;
	}

	public static Field setcC(Field cC) {
		AttributeHider.cC = cC;
		return cC;
	}
}
