package net.minegrid.obelisk.api.attributes;

import net.minegrid.obelisk.core.Foundation;

import org.bukkit.event.block.*;

import java.util.*;

import org.bukkit.event.*;
import org.bukkit.event.player.*;
import org.bukkit.entity.*;
import org.bukkit.command.*;
import org.bukkit.*;
import org.apache.commons.lang.*;
import org.bukkit.inventory.*;

/**
 * 
 * @author Pepijn Bakker
 *
 */
public final class CustomItem implements Listener
{
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents((Listener)this, Foundation.getInstance());
        AttributeHider.listen(Foundation.getInstance());
        Customizer.getUnplaceableItems().add("Unplaceable");
    }
    
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
    public void onPlace(final BlockPlaceEvent e) {
        final Set<String> unpl = Customizer.getUnplaceableItems();
        if (!unpl.isEmpty()) {
            final String tag = Customizer.getCustomTag(e.getItemInHand());
            if (tag != null && unpl.contains(tag)) {
                e.setCancelled(true);
            }
        }
    }
    
    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onCreative(final PlayerGameModeChangeEvent e) {
        if (e.getNewGameMode() == GameMode.CREATIVE) {
            final Player p = e.getPlayer();
            Bukkit.getScheduler().scheduleSyncDelayedTask(Foundation.getInstance(), (Runnable)new Runnable() {
                @Override
                public void run() {
                    p.updateInventory();
                }
            });
        }
    }
    
    public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("Hold an item to check.");
            return true;
        }
        final Player p = (Player)sender;
        ItemStack is = p.getItemInHand();
        if (is.getType() == Material.AIR) {
            sender.sendMessage("Hold an item to check.");
            return true;
        }
        if (args.length == 0) {
            sender.sendMessage("Custom Tag Found: " + Customizer.getCustomTag(is));
        }
        else {
            final String tag = StringUtils.join((Object[])args, ' ', 0, args.length);
            is = Customizer.giveCustomTag(is, tag);
            p.setItemInHand(is);
            sender.sendMessage("Custom Tag set: " + tag);
        }
        return true;
    }
}
