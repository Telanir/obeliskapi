package net.minegrid.obelisk.api.attributes;

import java.util.*;

import com.google.common.collect.*;

import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import net.minecraft.server.v1_8_R3.GenericAttributes;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;

/**
 * 
 * @author Pepijn Bakker
 *
 */
public class Customizer
{
	static final UUID CUSTOM_UUID;
	static final UUID RANDOM_UUID;
	private static final Set<String> UNPLACABLES;

	static {
		CUSTOM_UUID = UUID.fromString("fd2f5270-0471-11e4-9191-0800200c9a66");
		RANDOM_UUID = UUID.randomUUID();
		UNPLACABLES = Sets.newHashSet();
	}

	public static Set<String> getUnplaceableItems() {
		return Customizer.UNPLACABLES;
	}

	public static boolean makeUnplaceable(final String tag) {
		return Customizer.UNPLACABLES.add(tag);
	}

	public static ItemStack giveCustomTag(ItemStack is, final String tag) {
		if (hasAttributes(is)) {
			is = restoreVanillaAttribute(is);
		}
		final AttributeModifier mod = new AttributeModifier(Customizer.CUSTOM_UUID, tag, 0.0, Operation.INCREMENT, AttributeType.FOLLOW_RANGE);
		is = AttributeItem.addModifier(mod, is);
		return is;
	}

	public static ItemStack removeCustomTag(ItemStack is) {
		final AttributeModifier mod = new AttributeModifier(Customizer.CUSTOM_UUID, AttributeType.FOLLOW_RANGE);
		is = AttributeItem.removeModifier(mod, is);
		return is;
	}

	public static boolean isSameCustomItem(final ItemStack i1, final ItemStack i2) {
		if (i1 == null || i2 == null){
			if (i1 != null || i2 != null) return false;
			else return true;
		}
		
        if (i1.getType() != i2.getType()) {
            return false;
        }
        if ((i1.getType().getMaxDurability() <= 0 && i1.getDurability() != i2.getDurability())) {
        	return false;
        }
		
		final String t1 = getCustomTag(i1);
		final String t2 = getCustomTag(i2);
		
		if (t1 == null) {
			return t2 == null;
		}
		return t1.equals(t2);
	}

	public static boolean isCustom(final ItemStack is) {
		return getCustomTag(is) != null;
	}

	public static String getCustomTag(final ItemStack is) {
		final net.minecraft.server.v1_8_R3.ItemStack nms = CraftItemStack.asNMSCopy(is);
		if (nms.getTag() != null && nms.getTag().hasKeyOfType("AttributeModifiers", 9)) {
			final NBTTagList list = nms.getTag().getList("AttributeModifiers", 10);
			for (int i = 0; i < list.size(); ++i) {
				final NBTTagCompound ctag = list.get(i);
				final net.minecraft.server.v1_8_R3.AttributeModifier mod = GenericAttributes.a(ctag);
				if (Customizer.CUSTOM_UUID.equals(mod.a())) {
					return mod.b();
				}
			}
		}
		return null;
	}

	public static Map<String, Object> serialize(ItemStack is) {
		return AttributeItem.serialize(is);
	}

	public static ItemStack deserialize(Map<String, Object> args) {
		return AttributeItem.deserialize(args);
	}

	private static boolean hasAttributes(final ItemStack is) {
		final net.minecraft.server.v1_8_R3.ItemStack nms = CraftItemStack.asNMSCopy(is);
		return nms.getTag() != null && nms.getTag().hasKeyOfType("AttributeModifiers", 9);
	}

	private static ItemStack restoreVanillaAttribute(ItemStack is) {
		int dmg = 0;
		switch (is.getType()) {
		case DIAMOND_SWORD: {
			dmg = 7;
			break;
		}
		case IRON_SWORD:
		case DIAMOND_AXE: {
			dmg = 6;
			break;
		}
		case IRON_AXE:
		case STONE_SWORD:
		case DIAMOND_PICKAXE: {
			dmg = 5;
			break;
		}
		case IRON_PICKAXE:
		case WOOD_SWORD:
		case STONE_AXE:
		case DIAMOND_SPADE:
		case GOLD_SWORD: {
			dmg = 4;
			break;
		}
		case WOOD_AXE:
		case IRON_SPADE:
		case GOLD_AXE:
		case STONE_PICKAXE: {
			dmg = 3;
			break;
		}
		case WOOD_PICKAXE:
		case STONE_SPADE:
		case GOLD_PICKAXE: {
			dmg = 2;
			break;
		}
		case WOOD_SPADE:
		case GOLD_SPADE: {
			dmg = 1;
			break;
		}
		default:
			break;
		}
		if (dmg > 0) {
			final AttributeModifier mod = new AttributeModifier(Customizer.RANDOM_UUID, "ob_vanilladmg", (double)dmg, Operation.INCREMENT, AttributeType.ATTACK_DAMAGE);
			is = AttributeItem.addModifier(mod, is);
		}
		return is;
	}
}
