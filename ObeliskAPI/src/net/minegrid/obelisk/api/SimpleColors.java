package net.minegrid.obelisk.api;

import org.bukkit.ChatColor;

/**
 * Created by Maxim on 2016-03-04.
 */
public interface SimpleColors {
    String
        RED = ChatColor.RED.toString(),
        BLUE = ChatColor.BLUE.toString(),
        GREEN = ChatColor.GREEN.toString(),
        YELLOW = ChatColor.YELLOW.toString(),
        LIGHT_PURPLE = ChatColor.LIGHT_PURPLE.toString(),
        PURPLE = ChatColor.DARK_PURPLE.toString(),
        PINK = ChatColor.LIGHT_PURPLE.toString(),
        AQUA = ChatColor.AQUA.toString(),
        GRAY = ChatColor.GRAY.toString(),
        BLACK = ChatColor.BLACK.toString(),

        DARK_RED = ChatColor.DARK_RED.toString(),
        DARK_BLUE = ChatColor.DARK_BLUE.toString(),
        DARK_GREEN = ChatColor.DARK_GREEN.toString(),
        GOLD = ChatColor.GOLD.toString(),
        ORANGE = ChatColor.GOLD.toString(),
        DARK_PURPLE = ChatColor.DARK_PURPLE.toString(),
        DARK_AQUA = ChatColor.DARK_AQUA.toString(),
        DARK_GRAY = ChatColor.DARK_GRAY.toString(),

        WHITE = ChatColor.WHITE.toString(),
        BOLD = ChatColor.BOLD.toString(),
        ITALIC = ChatColor.ITALIC.toString(),
        UNDERLINE = ChatColor.UNDERLINE.toString(),
        RESET = ChatColor.RESET.toString(),
        MAGIC = ChatColor.MAGIC.toString(),
        STRIKE = ChatColor.STRIKETHROUGH.toString();
}
