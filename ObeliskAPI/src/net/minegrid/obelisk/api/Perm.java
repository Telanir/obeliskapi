package net.minegrid.obelisk.api;

import net.minegrid.obelisk.api.tools.UserTool;
import org.bukkit.command.CommandSender;

/**
 * Created by Maxim on 2016-02-28.
 */
public enum Perm {
    DEV("Developer", "obelisk.developer"),
    ADMIN("Administrator", "obelisk.administrator"),
    MOD("Moderator", "obelisk.moderator");

    String name;
    String permission;

    Perm(String name, String permission) {
        this.name = name;
        this.permission = permission;
    }

    public String getName() {
        return name;
    }

    public String getPermission() {
        return permission;
    }

    public boolean has(CommandSender commandSender) {
        return UserTool.hasPerm(commandSender, permission);
    }
}
