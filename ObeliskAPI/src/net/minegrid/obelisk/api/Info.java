package net.minegrid.obelisk.api;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bukkit.ChatColor;

import net.minegrid.obelisk.api.Module.DataClump;
import net.minegrid.obelisk.api.Module.ModuleType;

/**
 * The <code>Info</code> object is used to store data about a <code>Module</code>
 * in the Obelisk ecosystem.
 * 
 * The name should not contain any spaces.
 * <code>fileName</code> and <code>displayName</code> default to <code>name</code>,
 * where <code>fileName</code> will automatically add the .xml extension.
 * 
 * The <code>color</code> defines the theme of the <code>Module</code>.
 * 
 * Dependencies must be loaded and enabled before the <code>Module</code> can be enabled.
 * 
 * The <code>data</code> var offloads saved/loaded data to a specific class.
 * If <code>data</code> is not specified, Obelisk will instead save/load the entire 
 * <code>Module</code> with the <code>fileName</code>.
 * 
 * @version 1.0
 * @since 1.0.0
 * @author Maxim Chipeev
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Info {
	String name();
	ChatColor color();
	String version();
	ModuleType type() default ModuleType.MODULE;
	String fileName() default "";
	String displayName() default "";
	String info() default "";
	Class<? extends Module>[] dependencies() default {};
	Class<? extends DataClump> data() default DataClump.class;
}