package net.minegrid.obelisk.api;

import net.minegrid.obelisk.core.monitor.MonitorDaemon;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by Maxim on 2016-02-20.
 */
public enum Monitor {
    SYSTEM("{sys} "),
    SEVERE("[SEVERE] "),
    MINOR("[!] "),
    TRIVIAL;

    String prefix;

    Monitor() {
        prefix = "";
    }

    Monitor(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;
    }

    public static void sendHotbarMessage(Player p, String message) {
        MonitorDaemon.sendHotbarMessage(p, message);
    }

    public static Rule createRule() {
        return new Rule();
    }

    public static Rule.Builder buildRule() {
        return Rule.build();
    }

    public static Rule createRule(Parser[] parsers, MonitorSender[] senders) {
        return new Rule(parsers, senders);
    }

    public enum Template {
        CONSOLE,
        BROADCAST;

        public Rule create() {
            Rule r = createRule();
            switch(this) {
                case BROADCAST:
                    r.addSender(new MonitorSender.Broadcast(TRIVIAL));
                    break;
                default:
                    r.addSender(new MonitorSender.Single(TRIVIAL, Bukkit.getConsoleSender()));
            }
            return r;
        }
    }

    @SuppressWarnings("unchecked")
    public static class Rule {
        List<Parser> parsers;
        List<MonitorSender> senders;

        public Rule() {
            parsers = new ArrayList<>();
            senders = new ArrayList<>();

            MonitorDaemon.addRule(this);
        }

        private Rule(Parser[] parsers, MonitorSender[] senders) {
            this();
            for(Parser parser : parsers)
                appendParser(parser);
            for(MonitorSender sender : senders)
                addSender(sender);
        }

        public void pass(Monitor level, String message) {
            message = level.getPrefix() + message;
            for(Parser parser : parsers)
                message = parser.parse(message);
            for(MonitorSender sender : senders)
                if(level.ordinal() <= sender.getRank().ordinal())
                    sender.send(message);
        }

        public void trivial(String message) {
            pass(TRIVIAL, message);
        }

        public void severe(String message) {
            pass(SEVERE, message);
        }

        public void minor(String message) {
            pass(MINOR, message);
        }

        public void terminate() {
            parsers.forEach(Parser::terminate);
            senders.forEach(MonitorSender::terminate);
            parsers.clear();
            senders.clear();
            MonitorDaemon.removeRule(this);
        }

        public void addSender(MonitorSender sender) {
            senders.add(sender);
        }

        public void removeSender(MonitorSender sender) {
            senders.remove(sender);
        }

        public void removeParser(Parser parser) {
            parsers.remove(parser);
        }

        public void appendParser(Parser parser) {
            parsers.add(0, parser);
        }

        public void prependParser(Parser parser) {
            parsers.add(parser);
        }

        public Collection<Parser> getParsers() {
            return Collections.unmodifiableList(parsers);
        }

        public <T extends Parser> Collection<T> getParsers(Class<T> type) {
            Collection<T> parsers = new ArrayList<>();
            this.parsers.forEach(p -> {
                if(type.isInstance(p)) parsers.add((T)p);
            });
            return parsers;
        }

        public <T extends MonitorSender> Collection<T> getSenders(Class<T> type) {
            Collection<T> senders = new ArrayList<>();
            this.senders.forEach(p -> {
                if(type.isInstance(p)) senders.add((T)p);
            });
            return senders;
        }

        public Collection<MonitorSender> getSenders() {
            return Collections.unmodifiableList(senders);
        }

        public static Builder build() {
            return new Builder();
        }

        public static class Builder {
            Rule rule;
            private Builder() {
                rule = createRule();
            }

            public Builder parser(Parser parser) {
                rule.appendParser(parser);
                return this;
            }

            public Builder moduleParser(Module module) {
                return parser(ModuleParser.build(module));
            }

            public Builder timeParser() {
                return parser(TimeParser.build());
            }

            public Builder singleSender(Monitor level, CommandSender sender) {
                return sender(MonitorSender.Single.build(level, sender));
            }

            public Builder multiSender(Monitor level, CommandSender...senders) {
                return sender(MonitorSender.Multi.build(level, senders));
            }

            public Builder broadcastSender(Monitor level) {
                return sender(MonitorSender.Broadcast.build(level));
            }

            public Builder fileSender(Monitor level, File file) {
                return sender(MonitorSender.File.build(level, file));
            }

            public Builder groupSender(Monitor level, Collection<CommandSender> senders) {
                return sender(MonitorSender.Group.build(level, senders));
            }

            public Builder sender(MonitorSender sender) {
                rule.addSender(sender);
                return this;
            }

            public Rule create() {
                return rule;
            }
        }
    }

    @FunctionalInterface
    public interface Parser {
        String parse(String msg);
        default void terminate() {}
    }

    public static class ModuleParser implements Parser {
        Module module;

        ModuleParser(Module module) {
            this.module = module;
        }

        @Override
        public String parse(String msg) {
            return "["+module.getDisplayName()+"] " + msg;
        }

        public Module getModule() {
            return module;
        }

        public static ModuleParser build(Module module) {
            return new ModuleParser(module);
        }
    }

    public static class TimeParser implements Parser {
        @Override
        public String parse(String msg) {
            return "("+LocalTime.now().truncatedTo(ChronoUnit.SECONDS).format(DateTimeFormatter.ISO_TIME)+") "+msg;
        }

        public static TimeParser build() {
            return new TimeParser();
        }
    }
}
