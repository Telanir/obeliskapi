package net.minegrid.obelisk.api.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class ArrayTool {
	private ArrayTool() { throw new AssertionError(); }
	
	/**
	 * Adds an object to an array contained as a value safely.
	 * An array is created containing the object if one is not found.
	 * 
	 * @param map HashMap to add object to.
	 * @param key Key for the List.
	 * @param object Object to add to value.
	 */
	public static <K, V> void addToHashMapValueList(Map<K,List<V>> map, K key, V object) {
		if(map.containsKey(key))
			map.get(key).add(object);
		else {
			List<V> value = new ArrayList<V>();
			value.add(object);
			map.put(key, value);
		}
	}
	
	public static List<String> addPrefix(List<String> s, String prefix) {
		for(int i = 0; i < s.size(); i++)
			s.set(i, prefix+s.get(i));
		return s;
	}
	
	public static String[] stackTraceToArray(StackTraceElement[] stackTrace) {
		String[] trace = new String[stackTrace.length];
		for(int i = 0; i < stackTrace.length; i++)
			trace[i] = stackTrace[i].toString();
		return trace;
	}
}
