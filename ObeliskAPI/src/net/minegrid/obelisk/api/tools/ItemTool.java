package net.minegrid.obelisk.api.tools;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public final class ItemTool {
	private ItemTool() { throw new AssertionError(); }
	
	public static int countEmptySlots(Inventory i) {
		int slot = 0;
		for(ItemStack s : i)
			if(s == null ? true : s.getType() == Material.AIR)
				slot++;
		return slot;
	}
	
	public static int countEmptySlots(ItemStack[] i) {
		int slot = 0;
		for(ItemStack s : i)
			if(s == null ? true : s.getType() == Material.AIR)
				slot++;
		return slot;
	}
	
	public static int countEmptySlots(Object[] i) {
		int slot = 0;
		for(Object s : i)
			if(s == null)
				slot++;
		return slot;
	}
	
	public static boolean compare(ItemStack item1, ItemStack item2, boolean compareType, boolean compareDurability, boolean compareDisplayName, boolean compareLore) {
		if(compareType)
			if(item1.getType() != item2.getType())
				return false;
		if(compareDurability)
			if(item1.getDurability() != item2.getDurability())
				return false;
		if(compareDisplayName || compareLore) {
			if(item1.hasItemMeta() == item2.hasItemMeta()) {
				if(item1.hasItemMeta()) {
					ItemMeta m1 = item1.getItemMeta();
					ItemMeta m2 = item2.getItemMeta();
					if(compareDisplayName) {
						if(m1.hasDisplayName() == m2.hasDisplayName()) {
							if(m1.hasDisplayName()) {
								if(!m1.getDisplayName().equals(m2.getDisplayName()))
									return false;
							}
						} else
							return false;
					}
					if(compareLore) {
						if(m1.hasLore() == m2.hasLore()) {
							if(m1.hasLore())
								if(m1.getLore().size() == m2.getLore().size())
									for(int x = 0; x < m1.getLore().size(); x++) {
										if(!m1.getLore().get(x).equals(m2.getLore().get(x)))
											return false;
									}
								else
									return false;
						} else
							return false;
					}
				}
			} else
				return false;
		}
		return true;
	}
	
	public static boolean hasRoomFor(Player p, ItemStack s) {
		if(p.getInventory().firstEmpty() == -1 || s.getAmount() > 64) {
			if(getMaxStackSize(s.getType()) == 1)
				return false;

			int amnt = s.getAmount();
			for(ItemStack i : p.getInventory().getContents()) {
				if(i != null) {
					if(i.isSimilar(s)) {
						int space = getMaxStackSize(i.getType()) - i.getAmount();
						amnt -= space;
						if(amnt <= 0)
							break;
					}
				} else {
					int space = getMaxStackSize(s.getType());
					amnt -= space;
					if(amnt<=0)
						break;
				}
			}
			if(amnt > 0)
				return false;
		}
		return true;
	}

	public static void addItem(Player p, ItemStack s) {
		if(p.getInventory().firstEmpty() == -1 || s.getAmount() > 64) {
			int amnt = s.getAmount();
			for(ItemStack i : p.getInventory().getContents()) {
				if(i != null) {
					if(i.isSimilar(s)) {
						int space = getMaxStackSize(i.getType()) - i.getAmount();
						if(amnt > space) {
							i.setAmount(getMaxStackSize(i.getType()));
							amnt-=space;
						} else {
							i.setAmount(i.getAmount()+amnt);
							amnt = 0;
						}
						if(amnt <= 0)
							return;
					}
				} else {
					ItemStack add = s.clone();
					add.setAmount(getMaxStackSize(s.getType()));
					p.getInventory().addItem(add);
					
					int space = getMaxStackSize(s.getType());
					amnt -= space;
					if(amnt<=0)
						return;
				}
			}
			if(amnt > 0) {
				while(amnt > 64) {
					if(amnt > 64) {
						ItemStack add = s.clone();
						add.setAmount(64);
						p.getWorld().dropItem(p.getLocation(), add);
						amnt -= 64;
					} else {
						ItemStack add = s.clone();
						add.setAmount(amnt);
						p.getWorld().dropItem(p.getLocation(), add);
						amnt = 0;
					}
				}
			}
		} else
			p.getInventory().addItem(s);
	}

	public static ItemStack decreaseAmount(ItemStack i) {
		return decreaseAmount(i, 1);
	}

	public static ItemStack decreaseAmount(ItemStack i, int reduction) {
		if(i.getAmount() > reduction)
			i.setAmount(i.getAmount()-reduction);
		else
			i = null;
		return i;
	}
	
	public static boolean isFlora(Material m) {
		switch(m) {
		case YELLOW_FLOWER:
		case RED_ROSE:
		case DOUBLE_PLANT:
		case LONG_GRASS:
		case WATER_LILY:
		case CROPS:
		case VINE:
		case SUGAR_CANE_BLOCK:
		case POTATO:
		case CARROT:
		case BROWN_MUSHROOM:
		case RED_MUSHROOM:
		case COCOA:
		case SAPLING:
			return true;
		default:
			return false;
		}
	}
	
	public static boolean isCrop(Material m) {
		switch(m) {
		case CROPS:
		case CARROT:
		case POTATO:
			return true;
		default:
			return false;
		}
	}
	
	public static boolean isMaterial(Block m, Material...ms) {
		for(Material t : ms)
			if(t == m.getType())
				return true;
		return false;
	}
	
	public static boolean isMaterial(Material m, Material...ms) {
		for(Material t : ms)
			if(t == m)
				return true;
		return false;
	}
	
	
	
	public static void playSound(Location loc, Sound s, int distance, float volume, float pitch) {
		for(Player p : MobTool.getNearbyPlayers(loc, distance, false))
			p.playSound(p.getLocation(), s, volume, pitch);
	}
	
	public static boolean isCombatRelated(Material m) {
		switch(m) {
		case IRON_SWORD:
		case IRON_LEGGINGS:
		case IRON_BOOTS:
		case IRON_HELMET:
		case IRON_CHESTPLATE:
		case GOLD_SWORD:
		case GOLD_LEGGINGS:
		case GOLD_BOOTS:
		case GOLD_HELMET:
		case GOLD_CHESTPLATE:
		case DIAMOND_SWORD:
		case DIAMOND_LEGGINGS:
		case DIAMOND_BOOTS:
		case DIAMOND_HELMET:
		case DIAMOND_CHESTPLATE:
		case STONE_SWORD:
		case CHAINMAIL_LEGGINGS:
		case CHAINMAIL_BOOTS:
		case CHAINMAIL_HELMET:
		case CHAINMAIL_CHESTPLATE:
		case WOOD_SWORD:
		case LEATHER_LEGGINGS:
		case LEATHER_BOOTS:
		case LEATHER_HELMET:
		case LEATHER_CHESTPLATE:
			return true;
		default:
			return false;
		}
	}
	
	public static int getMaxStackSize(Material m) {
		switch(m) {
		case BUCKET:
		case SNOW_BALL:
		case EGG:
		case ENDER_PEARL:
		case SIGN:
			return 16;
		case FISHING_ROD:
		case IRON_SWORD:
		case IRON_HOE:
		case IRON_SPADE:
		case IRON_AXE:
		case IRON_PICKAXE:
		case IRON_HELMET:
		case IRON_CHESTPLATE:
		case IRON_LEGGINGS:
		case IRON_BOOTS:
		case WOOD_SWORD:
		case WOOD_PICKAXE:
		case WOOD_AXE:
		case WOOD_HOE:
		case WOOD_SPADE:
		case LEATHER_HELMET:
		case LEATHER_CHESTPLATE:
		case LEATHER_LEGGINGS:
		case LEATHER_BOOTS:
		case GOLD_SWORD:
		case GOLD_SPADE:
		case GOLD_PICKAXE:
		case GOLD_AXE:
		case GOLD_HOE:
		case GOLD_HELMET:
		case GOLD_CHESTPLATE:
		case GOLD_LEGGINGS:
		case GOLD_BOOTS:
		case DIAMOND_SWORD:
		case DIAMOND_HOE:
		case DIAMOND_SPADE:
		case DIAMOND_PICKAXE:
		case DIAMOND_AXE:
		case DIAMOND_HELMET:
		case DIAMOND_CHESTPLATE:
		case DIAMOND_LEGGINGS:
		case DIAMOND_BOOTS:
		case MUSHROOM_SOUP:
		case SHEARS:
		case POTION:
		case BOW:
		case WOOD_DOOR:
		case IRON_DOOR:
		case FLINT_AND_STEEL:
		case STONE_SWORD:
		case STONE_AXE:
		case STONE_SPADE:
		case STONE_PICKAXE:
		case STONE_HOE:
		case MINECART:
		case STORAGE_MINECART:
		case EXPLOSIVE_MINECART:
		case HOPPER_MINECART:
		case WATER_BUCKET:
		case LAVA_BUCKET:
		case BOAT:
		case MILK_BUCKET:
		case SADDLE:
		case BED:
		case CARROT_STICK:
		case IRON_BARDING:
		case GOLD_BARDING:
		case DIAMOND_BARDING:
		case BOOK_AND_QUILL:
		case MAP:
		case ENCHANTED_BOOK: //ARCHE: Please leave one this at max stack 1
		case GOLD_RECORD:
		case GREEN_RECORD:
		case RECORD_3:
		case RECORD_4:
		case RECORD_5:
		case RECORD_6:
		case RECORD_7:
		case RECORD_8:
		case RECORD_9:
		case RECORD_10:
		case RECORD_11:
		case RECORD_12:
		case WRITTEN_BOOK:
			return 1;
		default:
			return 64;
		}
	}
	
	public static String magicSequenceBegin() {
		return ChatColor.RED+" "+ChatColor.MAGIC;
	}

	public static String longToMagic(long num) {
		String s = Long.toString(num);

		s = s.replaceAll("1", " "+ChatColor.BOLD);
		s = s.replaceAll("2", " "+ChatColor.GREEN);
		s = s.replaceAll("3", " "+ChatColor.DARK_BLUE);
		s = s.replaceAll("4", " "+ChatColor.AQUA);
		s = s.replaceAll("5", " "+ChatColor.RED);
		s = s.replaceAll("6", " "+ChatColor.WHITE);
		s = s.replaceAll("7", " "+ChatColor.YELLOW);
		s = s.replaceAll("8", " "+ChatColor.DARK_GREEN);
		s = s.replaceAll("9", " "+ChatColor.GRAY);
		s = s.replaceAll("0", " "+ChatColor.ITALIC);

		s = magicSequenceBegin() + " " + s;
		return s;
	}

	public static Long magicToLong(String s) {
		String r = s;

		r = r.replaceFirst(magicSequenceBegin()+" ", "");

		r = r.replaceAll(""+ChatColor.BOLD, "1");

		r = r.replaceAll(""+ChatColor.GREEN, "2");

		r = r.replaceAll(""+ChatColor.DARK_BLUE, "3");

		r = r.replaceAll(""+ChatColor.AQUA, "4");

		r = r.replaceAll(""+ChatColor.RED, "5");

		r = r.replaceAll(""+ChatColor.WHITE, "6");

		r = r.replaceAll(""+ChatColor.YELLOW, "7");

		r = r.replaceAll(""+ChatColor.DARK_GREEN, "8");

		r = r.replaceAll(""+ChatColor.GRAY, "9");

		r = r.replaceAll(""+ChatColor.ITALIC, "0");

		r = r.replaceAll(" ", "");

		return Long.valueOf(r);
	}
}
