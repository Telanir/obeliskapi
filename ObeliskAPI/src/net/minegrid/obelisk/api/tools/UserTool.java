package net.minegrid.obelisk.api.tools;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import net.minegrid.obelisk.api.tools.json.ChatBoxAction;
import net.minegrid.obelisk.api.tools.json.ChatMessage;
import net.minegrid.obelisk.api.tools.json.JsonMessage;
import net.minegrid.obelisk.core.Foundation;

import org.bukkit.OfflinePlayer;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.command.ColouredConsoleSender;
import org.bukkit.entity.Player;

import static org.bukkit.ChatColor.*;

public final class UserTool {
	private UserTool() { throw new AssertionError(); }
	
	public final static String perm_mod = "obelisk.moderator";
	
	public static boolean isVanished(Player p) {
		if (p.hasMetadata("vanished")) {
			if (p.getMetadata("vanished").get(0).asBoolean()) {
				return true;
			}
		}
		return false;
	}
	
	public static Player getPlayer(UUID id) {
		return Foundation.getServerInstance().getPlayer(id);
	}
	
	public static Player getPlayer(String s) {
		return Foundation.getServerInstance().getPlayer(s);
	}
	
	public static UUID getUUID(String name) {
		try {
			return UUIDTool.getUUIDOf(name);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	public static OfflinePlayer getOfflinePlayer(String s) {
		return Foundation.getServerInstance().getOfflinePlayer(s);
	}
	
	public static List<Player> getPlayers(UUID...names) {
		List<Player> players = new ArrayList<Player>();
		for(UUID s : names){
			Player ps = Foundation.getServerInstance().getPlayer(s);
			if(ps!=null)players.add(ps);
		}
		return players;
	}

	public static OfflinePlayer getOfflinePlayer(UUID id) {
		return Foundation.getServerInstance().getOfflinePlayer(id);
	}

	public static List<OfflinePlayer> getOfflinePlayers(UUID...names) {
		List<OfflinePlayer> players = new ArrayList<OfflinePlayer>();
		for(UUID s : names){
			OfflinePlayer ps = Foundation.getServerInstance().getOfflinePlayer(s);
			if(ps!=null)players.add(ps);
		}
		return players;
	}
	
	public enum ObeliskPermissionLevel {
		// Hierarchy user -> admin -> developer
		DEVELOPER(0), ADMIN(1), MODERATOR(2), USER(3);
		
		private int weight;
		
		ObeliskPermissionLevel(int weight) {
			this.weight = weight;
		}
		
		public static ObeliskPermissionLevel getLevel(CommandSender cs) {
			if(cs instanceof Server) {
				return DEVELOPER;
			} else {
				Player p = (Player) cs;
				if(isDeveloper(p.getUniqueId())) {
					return DEVELOPER;
				} else if(isAdministrator(p.getUniqueId())) {
					return ADMIN;
				} else {
					return USER;
				}
			}
		}
		
		public static boolean hasPermissionAtMinimum(CommandSender cs, ObeliskPermissionLevel level) {
			return getLevel(cs).weight <= level.weight;
		}
	}
	
	public static List<String> namesFromUUIDs(List<UUID> ids) {
		List<String> n = new ArrayList<>();
		for(UUID id : ids) {
			OfflinePlayer p = getOfflinePlayer(id);
			n.add(p.getName());
		}
		return n;
	}
	
	public static List<UUID> uuidsFromNames(List<String> n) {
		List<UUID> ids = new ArrayList<>();
		for(String id : n) {
			OfflinePlayer p = getOfflinePlayer(id);
			if(p!=null)
				ids.add(p.getUniqueId());
		}
		return ids;
	}
	
	private static UUID[] administrators = getAdmins();
	private static UUID[] devs = getDevs();
	
	public static boolean isAdministrator(UUID id) {
		for(UUID s : administrators)
			if(s.equals(id))
				return true;
		return isDeveloper(id);
	}
	
	public static boolean isDeveloper(UUID id) {
		for(UUID s : devs)
			if(s.equals(id))
				return true;
		return false;
	}
	
	private static UUID[] getAdmins() {
		return new UUID[]{
				UUID.fromString("8d9a86f9-f044-49eb-bcf3-8dbb4bb5cd70"), 	//__Xan__ (Tsuyose)
				UUID.fromString("8145d601-8b3c-4f08-b93b-542a6380c2f9"), 	//Tsuyose
				UUID.fromString("68770e6e-a4b7-44c9-8a0d-61eb81f3dd91"), 	//Ari_Horen (Tsuyose)
				UUID.fromString("67e8ed55-4579-42cf-9514-0c0e1143d89d"), 	//Kovuudles (Readicti)
				UUID.fromString("d2b982db-4eec-4669-966b-ad70670ea4f1"),	//Readicti
				UUID.fromString("6b98dae5-e292-4664-b2b3-254c15567fad"), 	//_Urasept_
				UUID.fromString("cc459883-877d-42d5-a32f-f3a765febc12")		//Pandann
		};
	}

	private static UUID[] getDevs() {
		return new UUID[] {
				UUID.fromString("561637ba-06f6-4457-9787-ba65768c1b73"), 	//Tythus
				UUID.fromString("5328410e-c1c5-4557-8457-f30ea4b935aa"), 	//Telanir
				UUID.fromString("2b8176ac-89fc-47c8-99a5-4ed206380c2b"),	//501warhead
				UUID.fromString("0c4846c1-975f-493b-b931-91d725125e0f")		//Theryn (501warhead)
		};
	}
	
	public static void sendInfo(CommandSender cs, InfoMessage msg) {
		sendInfo(cs, msg, null);
	}
	
	public static void sendInfo(CommandSender cs, InfoMessage msg, String hover) {
		if(cs instanceof Player) {
			ChatMessage m = new JsonMessage(msg.getMessage());
			if(hover != null)
				m.setHoverEvent(ChatBoxAction.SHOW_TEXT, hover);
			m.sendTo((Player)cs);
		} else {
			cs.sendMessage(msg.getMessage());
		}
	}
	
	public enum InfoMessage {
		NOCONSOLE(RED+"Console cannot use this command."), 
		NOACCESS(RED+"You are not authorized to use this command."),
		ERROR(RED+"We have encountered an error! Please contact a Developer about this issue."),
		CMDNOTFOUND(GRAY+"Invalid command. Please check your spelling."),
		PLAYERNOTFOUND(GRAY+"Player could not be found.");
		
		String info;
		
		InfoMessage(String info) {
			this.info = info;
		}
		
		String getMessage() {
			return info;
		}
	}
	
	public static boolean isMod(CommandSender cs) {
		return hasPerm(cs, perm_mod);
	}
	
	/**
	 * 
	 * @param Command Sender can be player or server
	 * @param Permission that needs to be checked  ""
	 * @return
	 */
	public static boolean hasModSubPerm(CommandSender cs, String perm) {
		return hasPerm(cs, perm_mod + "." + perm);
	}
	
	/**
	 * 
	 * @param cs
	 * @param perm
	 * @return
	 */
	public static boolean hasPerm(CommandSender cs, String perm) {
		if(perm == null || perm.length() == 0)
			return true;
		if(cs instanceof ColouredConsoleSender) {
			return true;
		} else {
			Player p = (Player) cs;
			if(perm.equalsIgnoreCase("obelisk.administrator"))
				return UserTool.isAdministrator(p.getUniqueId());
			else if(perm.equalsIgnoreCase("obelisk.developer"))
				return UserTool.isDeveloper(p.getUniqueId());
			else {
				return p.hasPermission(perm);
			}
		}
	}
}
