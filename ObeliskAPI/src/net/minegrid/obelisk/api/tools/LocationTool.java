package net.minegrid.obelisk.api.tools;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

public final class LocationTool {
	private LocationTool() { throw new AssertionError(); }
	
	/**
	 * Multi-world friendly distance check between two locations.
	 * 
	 * @param l
	 *            First location.
	 * @param l2
	 *            Second location.
	 * @param dist
	 *            Distance between the two.
	 * @return Returns true if within same world and distance is less than or
	 *         equal to given distance.
	 */
	public static boolean withinDistance(Location l, Location l2, double dist) {
		if (l.getWorld().equals(l2.getWorld()))
			if (l.distance(l2) <= dist)
				return true;
		return false;
	}
	
	public boolean areLocationsEqualXYZ(Location l, Location t) {
		return (l.getX() == t.getX() && l.getY() == t.getY() && l.getZ() == t.getZ());
	}
	
	/**
	 * Get's a Location based off of where the entity is currently looking.
	 * 
	 * @param le
	 *            LivingEntity in question.
	 * @param accuracy
	 *            How much to move every time you check for blocks.
	 * @return location of block is returned.
	 */
	public static Location getTargetLocation(LivingEntity le, int iterations) {
		Location l = le.getEyeLocation().clone();
		float yaw = l.getYaw();
		float pitch = l.getPitch();

		Vector v = MobTool.getDirectionVector(yaw, pitch).divide(new Vector(2.0, 2.0, 2.0));
		double i = 0;
		while (i < iterations) {
			i++;
			l.add(v);
			if (!l.getBlock().isEmpty()) {
				break;
			}
		}

		return l;
	}

	public static Location getTargetLocation(Location l, double multiplier) {
		float yaw = l.getYaw();
		float pitch = l.getPitch();

		Vector v = MobTool.getDirectionVector(yaw, pitch).multiply(multiplier);

		return l.add(v);
	}

	public static Location getTargetLocation(LivingEntity le, double multiplier) {
		Location l = le.getEyeLocation().clone();
		float yaw = l.getYaw();
		float pitch = l.getPitch();

		Vector v = MobTool.getDirectionVector(yaw, pitch).multiply(multiplier);

		return l.add(v);
	}
	
	public static Location[] getTargetLocationLink(LivingEntity le, int iterations) {
		Location l = le.getEyeLocation().clone();
		float yaw = l.getYaw();
		float pitch = l.getPitch();
		
		List<Location> loc = new ArrayList<>();
		
		Vector v = MobTool.getDirectionVector(yaw, pitch);
		double i = 0;
		while (i < iterations) {
			i++;
			l.add(v);
			loc.add(l.clone());
			if (!l.getBlock().isEmpty()) {
				break;
			}
		}

		return loc.toArray(new Location[0]);
	}
	
	public static Location[] getLocationLink(Location le, Location lel) {
		Location l = MobTool.locationLookAt(le, lel);
		List<Location> loc = new ArrayList<>();
		
		float yaw = l.getYaw();
		float pitch = l.getPitch();
		
		Vector v = MobTool.getDirectionVector(yaw, pitch);
		while (l.distance(lel) < 1) {
			l.add(v);
			loc.add(l.clone());
			if (!l.getBlock().isEmpty()) {
				break;
			}
		}

		return loc.toArray(new Location[0]);
	}
}
