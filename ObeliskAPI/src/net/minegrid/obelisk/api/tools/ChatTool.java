package net.minegrid.obelisk.api.tools;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Splitter;
import net.minegrid.obelisk.api.SimpleColors;
import net.minegrid.obelisk.core.Foundation;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public final class ChatTool implements SimpleColors {
	private ChatTool() { throw new AssertionError(); }
	
	public static List<Player> emote(Location l, double radius, ChatColor color, String msg) {
		List<Player> heard = new ArrayList<Player>();
		for(Player p : MobTool.getNearbyPlayers(l, radius, false)) {
			p.sendMessage(color+"[!] " + msg);
			heard.add(p);
		}
		return heard;
	}
	
	public static List<Player> emote(Location l, double radius, String msg) {
		List<Player> heard = new ArrayList<Player>();
		for(Player p : MobTool.getNearbyPlayers(l, radius, false)) {
			p.sendMessage(ChatColor.YELLOW+"[!] " + msg);
			heard.add(p);
		}
		return heard;
	}

	public static Message msg(int limit) {
		return new Message(limit);
	}

	public static Message msg() {
		return new Message(-1);
	}

	public static class Message {
		List<String> contents;
		String format;
		int max_chars;

		Message(int limit) {
			this.max_chars = limit;
			this.contents = new ArrayList<>();
			this.format = "";
		}

		public Message bold() {
			format += BOLD;
			return this;
		}

		public Message em() {
			format += ITALIC;
			return this;
		}

		public Message strike() {
			format += STRIKE;
			return this;
		}

		public Message under() {
			format += UNDERLINE;
			return this;
		}

		public Message decol() {
			format = "";
			return this;
		}

		public Message col(ChatColor color) {
			format = color.toString();
			return this;
		}

		public Message col(String color) {
			format = color;
			return this;
		}

		public Message word(String text) {
			String last = contents.isEmpty() ? "" : contents.get(contents.size()-1);
			if(!contents.isEmpty() && (max_chars < 1 || last.length() + text.length() <= max_chars))
				contents.set(contents.size()-1, last + text);
			else
				contents.add(format+text);
			return this;
		}

		public Message text(String text) {
			if(text == null || text.length() == 0)
				return this;
			String[] words = text.split("\\s+");
			StringBuilder builder = new StringBuilder(format);

			for(String w : words) {
				if(max_chars < 1 || (builder.length() + w.length() <= max_chars) || (contents.isEmpty() && builder.length() == 0)) {
					builder.append(w).append(' ');
				} else {
					contents.add(builder.toString().trim());
					builder = new StringBuilder(format);
					builder.append(w);
				}
			}
			String last = builder.toString();
			if(!ChatColor.stripColor(last).isEmpty())
				contents.add(last.trim());
			return this;
		}

		public Message option(boolean b, String text) {
			if(b) text(text);
			return this;
		}

		public Message fork(boolean b, String text, String alt) {
			return b ? text(text) : text(alt);
		}

		public Message spacer() {
			contents.add("");
			return this;
		}

		public List<String> toList() {
			return contents;
		}

		public String toMultilineString() {
			return String.join("\n", contents);
		}

		public String[] toArray() {
			return contents.toArray(new String[contents.size()]);
		}
	}
	
	public static void broadcastMessage(String message, String permission) {
		for(Player p : Bukkit.getOnlinePlayers()) {
			if(UserTool.hasPerm(p, permission)) {
				p.sendMessage(message);
			}
		}
	}
	
	public static String location(Location l) {
		return "(" + l.getBlockX() + ", " + l.getBlockY() + " ," + l.getBlockZ() + ")";
	}
	
	/**
	 * Returns an array of strings that are no longer than max_size.
	 * @param text string to split up.
	 * @param max_size how long should each string be.
	 */
	public static String[] splitStringIntoSections(String text, int max_size) {
		List<String> t = new ArrayList<>();
		int c = 0;
		while(c < text.length()) {
			t.add(text.substring(c, c+max_size > text.length() ? text.length() : c+max_size));
			c += max_size;
		}
		return t.toArray(new String[0]);
	}
	
	/**
	  * Sends player a message delayed by 'x' seconds.
	  */
	public static void sendMessageWithDelay(final Player p, final String msg, long seconds) {
		if(p==null || msg == null)
			return;
		Foundation.getServerInstance().getScheduler().scheduleSyncDelayedTask(Foundation.getInstance(), () -> {
			if(p != null && msg != null) {
				if(p.isOnline()) {
					p.sendMessage(msg);
				}
			}
		}, seconds*20L);
	}
	
	/**
	 * Typically used with Player-Chosen colors 
	 */
	public static boolean isColorAllowed(ChatColor c) {
		switch (c) {
		case MAGIC:
		case BOLD:
		case ITALIC:
		case UNDERLINE:
		case BLACK:
		case DARK_GRAY:
		case GRAY:
		case WHITE:
		case RESET:
		case STRIKETHROUGH:
			return false;
		default:
			return true;
		}
	}
}
