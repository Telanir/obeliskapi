package net.minegrid.obelisk.api.tools;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.Validate;

/**
 * Decouples a HashMap in no particular order, but guarantees that keys and values
 * will retain their order.
 * The value in this class lies mainly in keeping code clean and swift.
 * 
 * @author Telanir
 *
 * @param <K> Decoupled Keys
 * @param <V> Decoupled Values
 */
public class DecoupledHashMap<K extends Object,V extends Object> {
	public List<K> keys;
	public List<V> values;

	public DecoupledHashMap(Map<K,V> map) {
		keys = new ArrayList<>();
		values = new ArrayList<>();
		for(Entry<K,V> e : map.entrySet()) {
			keys.add(e.getKey());
			values.add(e.getValue());
		}
	}

	/**
	 * Pairs the given keys and values to complete a HashMap.
	 * @return HashMap with keys as the first parameter and values as the second parameter.
	 */
	public static <K extends Object,V extends Object> Map<K,V> coupleHashMap(List<K> keys, List<V> values) {
		Map<K,V> map = new HashMap<>();
		Validate.isTrue(keys.size() == values.size(), "CoupleHashMap cannot couple two Lists of unequal size.");
		for(int i = 0; i < keys.size(); i++)
			map.put(keys.get(i), values.get(i));
		return map;
	}
}