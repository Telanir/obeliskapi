package net.minegrid.obelisk.api.tools;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

public final class DateTool {
	private DateTool() { throw new AssertionError(); }
	
	public static long getTimeLongMillis() {
		return new Date().getTime();
	}
	
	public static long getTimeLongHours() {
		return getTimeLongMillis() / (1000 * 60 * 24);
	}
	
	public static long getTimeLong() {
		return getTimeLongMillis() / 1000;
	}
	
	public static Map<TimeAmount, Integer> splitTime(long time) {
		Map<TimeAmount, Integer> list = new LinkedHashMap<>();
		TimeAmount[] values = TimeAmount.values();
		for(int i = values.length-1; i >= 0; i--){
			long seconds = values[i].seconds;
			if(time >= seconds) {
				long times = time / seconds;
				list.put(values[i], (int)times);
				time = time % seconds;
			}
		}
		return list;
	}
	
	/**
	 * Expects a dd/mm/yyyy string.
	 */
	public static LocalDate dateFromString(String s) {
		return LocalDate.parse(s, DateTimeFormatter.ofPattern("dd/MM/yyyy"));	
	}
	
	public static LocalDate currentDate() {
		return Instant.now().atZone(ZoneId.systemDefault()).toLocalDate();
	}
	
	public enum TimeAmount {
		SECOND(1L, "second", "s"),
		MINUTE(60L, "minute", "m"),
		HOUR(60*60L, "hour", "h"),
		DAY(60*60*24L, "day", "d"),
		YEAR(60*60*24*365L, "year", "y");
		
		String name;
		String shorthand;
		long seconds;
		
		TimeAmount(long s, String n, String t) {
			this.seconds = s;
			this.shorthand = t;
			this.name = n;
		}
		
		public String getName() {
			return name;
		}
		
		public String getShorthand() {
			return shorthand;
		}
		
		public long seconds(long count) {
			return getSeconds() * count;
		}
		
		public long ticks(long count) {
			return getTicks() * count;
		}
		
		public long getSeconds() {
			return seconds;
		}
		
		public long getTicks() {
			return seconds * 20;
		}
	}
}
