package net.minegrid.obelisk.api.tools;

import java.io.File;
import java.io.PrintWriter;
import java.util.List;

public final class FileTool {
	private FileTool() { throw new AssertionError(); }
	
	public static boolean writeToFlatfile(File file, List<String> lines) {
		try {
			file.createNewFile();
			PrintWriter writer = new PrintWriter(file);
			for(String line : lines)
				writer.println(line);
			writer.close();
		} catch(Exception e) {e.printStackTrace();}
		return false;
	}
}
