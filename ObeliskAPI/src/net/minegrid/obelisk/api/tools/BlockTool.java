package net.minegrid.obelisk.api.tools;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import net.minegrid.obelisk.api.blocks.WeakLocation;

public final class BlockTool {
	private BlockTool() { throw new AssertionError(); }
	
	public static Point getChunkLocation(Location l) {
		return new Point(l.getBlockX() << 4, l.getBlockZ() << 4);
		//return new Point((int)(Math.floor((double)l.getBlockX() / 16.0D)), (int)(Math.floor((double)l.getBlockZ() / 16.0D)));
	}
	
	public static Point getChunkLocation(WeakLocation l) {
		return new Point(l.getX() << 4, l.getZ() << 4);
		//return new Point((int)(Math.floor((double)l.getX() / 16.0D)), (int)(Math.floor((double)l.getZ() / 16.0D)));
	}
	
	/**
	 * Returns a list of blocks gathered from all dimensions
	 * within distance.
	 * @param l location to gather blocks from.
	 * @param dist distance in blocks to get blocks.
	 * @return returns a primitive array of blocks.
	 */
	public static Block[] getBlocksInDistanceSquare(Location l, int dist) {
		Block btemp = l.clone().getWorld().getBlockAt(l);
		Location t = btemp.getLocation();
		List<Block> blocks = new ArrayList<Block>();

		for(int ix = -dist; ix <= dist; ix++)
			for(int iy = -dist; iy <= dist; iy++)
				for(int iz = -dist; iz <= dist; iz++)
					blocks.add(t.getWorld().getBlockAt(new Location(t.getWorld(), t.getX()+ix, t.getY()+iy, t.getZ()+iz)));
		Block[] b = blocks.toArray(new Block[blocks.size()]);
		return b;
	}
	
	public static Block[] getBlocksInDistance(Location l, int dist) {
		Block btemp = l.clone().getWorld().getBlockAt(l);
		Location t = btemp.getLocation();
		List<Block> blocks = new ArrayList<Block>();

		for(int ix = -dist; ix <= dist; ix++)
			for(int iy = -dist; iy <= dist; iy++)
				for(int iz = -dist; iz <= dist; iz++)
					if(new Location(t.getWorld(), t.getX()+ix, t.getY()+iy, t.getZ()+iz).distance(t) <= dist)
						blocks.add(t.getWorld().getBlockAt(new Location(t.getWorld(), t.getX()+ix, t.getY()+iy, t.getZ()+iz)));
		Block[] b = blocks.toArray(new Block[blocks.size()]);
		return b;
	}

	/**
	 * Returns a list of blocks gathered from x, and z dimensions
	 * within distance. 'y' is grabbed from location.
	 * @param l location to gather blocks from.
	 * @param dist distance in blocks to get blocks.
	 * @return returns a primitive array of blocks.
	 */
	public static Block[] getBlockSquareInDistance(Location l, int dist, int height) {
		Block btemp = l.clone().getWorld().getBlockAt(l);
		Location t = btemp.getLocation();
		List<Block> blocks = new ArrayList<Block>();

		for(int ix = -dist; ix <= dist; ix++)
			for(int iz = -dist; iz <= dist; iz++)
				for(int iy = -height; iy <= height; iy++)
					blocks.add(t.getWorld().getBlockAt(new Location(t.getWorld(), t.getX()+ix, t.getY()+iy, t.getZ()+iz)));
		Block[] b = blocks.toArray(new Block[0]);
		return b;
	}

	/**
	 * Returns a list of blocks gathered from x, and z dimensions
	 * within distance. 'y' is grabbed from location.
	 * @param l location to gather blocks from.
	 * @param dist distance in blocks to get blocks.
	 * @return returns a primitive array of blocks.
	 */
	public static Block[] getBlockSquareInDistance(Location l, int dist) {
		Block btemp = l.clone().getWorld().getBlockAt(l);
		Location t = btemp.getLocation();
		List<Block> blocks = new ArrayList<Block>();

		for(int ix = -dist; ix <= dist; ix++)
			for(int iz = -dist; iz <= dist; iz++)
				blocks.add(t.getWorld().getBlockAt(new Location(t.getWorld(), t.getX()+ix, t.getY(), t.getZ()+iz)));
		Block[] b = blocks.toArray(new Block[blocks.size()]);
		return b;
	}

	public static Block[] getBlockDiskInDistance(Location l, int dist) {
		Block btemp = l.clone().getWorld().getBlockAt(l);
		Location t = btemp.getLocation();
		List<Block> blocks = new ArrayList<Block>();

		for(int ix = -dist; ix <= dist; ix++)
			for(int iz = -dist; iz <= dist; iz++)
				if(l.distance(new Location(t.getWorld(), t.getX()+ix, t.getY(), t.getZ()+iz)) <= dist)
					blocks.add(t.getWorld().getBlockAt(new Location(t.getWorld(), t.getX()+ix, t.getY(), t.getZ()+iz)));
		Block[] b = blocks.toArray(new Block[blocks.size()]);
		return b;
	}

	public static Block[] getBlockDiskInDistance(Location l, int dist, int height) {
		Block btemp = l.clone().getWorld().getBlockAt(l);
		Location t = btemp.getLocation();
		List<Block> blocks = new ArrayList<Block>();

		for(int ix = -dist; ix <= dist; ix++)
			for(int iz = -dist; iz <= dist; iz++)
				for(int iy = -height; iy <= height; iy++)
					if(l.distance(new Location(t.getWorld(), t.getX()+ix, t.getY()+iy, t.getZ()+iz)) <= dist)
						blocks.add(t.getWorld().getBlockAt(new Location(t.getWorld(), t.getX()+ix, t.getY()+iy, t.getZ()+iz)));
		Block[] b = blocks.toArray(new Block[blocks.size()]);
		return b;
	}
	
	public static Block getTopBlock(Location l) {
		Location x = l.clone();
		while (x != null && (x.getBlock().isEmpty() || !x.getBlock().getType().isSolid())) {
			if(x.getBlockY() < 1)
				return null;
			Block b = x.getBlock().getRelative(BlockFace.DOWN);
			if(b==null)
				break;
			x = b.getLocation();
		}
		return x.getBlock();
	}
	
	/**
	 * Returns height in blocks.
	 * 
	 * @param l
	 *            Starting location.
	 * @return Location is 0 if it is solid, automatically 1 if not, increments
	 *         after checking blocks below.
	 */
	public static int getHeight(Location l) {
		int h = 0;
		Location x = l.clone();
		while (x.getBlock().isEmpty()) {
			h++;
			Block b = x.getBlock().getRelative(BlockFace.DOWN);
			if(b==null)
				break;
			x = b.getLocation();
		}
		return h;
	}

	/**
	 * Returns the height in blocks.
	 * @param l Starting location.
	 * @param ms blocks to exclude, air automatically excluded
	 */
	public static int getHeight(Location l, Material...ms) {
		int h = 0;
		Location x = l;

		boolean exit = false;
		while(!exit) {
			exit = !x.getBlock().isEmpty();
			for(Material m : ms)
				if(x.getBlock().getType() == m) {
					exit = false;
					break;
				}
			h++;
			x = x.getBlock().getRelative(BlockFace.DOWN).getLocation();
		}
		return h;
	}

	/**
	 * Returns depth in blocks.
	 * 
	 * @param l Starting location.
	 * @return Location is 0 if it is empty, automatically 1 if it is not. Increments
	 * after checking blocks above.
	 */
	public static int getDepth(Location l) {
		int d = 0;
		Location x = l.clone();
		while(!x.getBlock().isEmpty()) {
			d++;
			x = x.getBlock().getRelative(BlockFace.UP).getLocation();
		}
		return d;
	}

	/**
	 * Returns depth in blocks.
	 * 
	 * @param l Starting location.
	 * @return Location is 0 if it is empty, automatically 1 if it is not. Increments
	 * after checking blocks above.
	 */
	public static int getDepth(Location l, Material...ms) {
		int d = 0;
		Location x = l.clone();
		boolean exit = false;
		while(!exit) {
			exit = !x.getBlock().isEmpty();
			for(Material m : ms)
				if(x.getBlock().getType() == m) {
					exit = false;
					break;
				}
			d++;
			x = x.getBlock().getRelative(BlockFace.UP).getLocation();
		}
		return d;
	}
}
