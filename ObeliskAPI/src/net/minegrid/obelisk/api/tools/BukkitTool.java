package net.minegrid.obelisk.api.tools;

import org.bukkit.Bukkit;

import net.minegrid.obelisk.core.Foundation;

public final class BukkitTool {
	private BukkitTool() { throw new AssertionError(); }
	
	public static final String PATH_NMS;
	public static final String PATH_BKT;
	public static final String VERSION;
	
	static {
		VERSION = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",")[3] + ".";
		PATH_NMS = "net.minecraft.server." + VERSION;
		PATH_BKT = "org.bukkit.craftbukkit." + VERSION;
	}
	
	public static Class<?> bukkitClass(String extension) {
		try {
			return Class.forName(PATH_BKT + extension);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Class<?> nmsClass(String extension) {
		try {
			return Class.forName(PATH_NMS + extension);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Performs a task after a certain time.
	 *
	 * If Foundation is disabled, performs the task instantly.
	 * @param time Time in ticks.
	 * @param r Action to process.
     */
	public static void delayPerform(long time, Runnable r) {
		if(Foundation.getInstance().isEnabled())
	        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(Foundation.getInstance(), r, time);
		else
			r.run();
    }

	/**
	 * Performs a task after a certain time in a separate thread.
	 *
	 * If Foundation is disabled, performs the task instantly.
	 * @param time Time in ticks.
	 * @param r Action to process.
	 */
	public static void delayPerformAsync(long time, Runnable r) {
		if(Foundation.getInstance().isEnabled())
			Bukkit.getServer().getScheduler().runTaskLaterAsynchronously(Foundation.getInstance(), r, time);
		else
			r.run();
	}
}
