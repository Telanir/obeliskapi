package net.minegrid.obelisk.api.tools;

import static java.lang.Math.cos;
import static java.lang.Math.sin;
import static java.lang.Math.toRadians;

import java.util.*;

import net.minecraft.server.v1_8_R3.AxisAlignedBB;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Firework;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;

public final class MobTool {
	private MobTool() { throw new AssertionError(); }
	
	public static Player getNearestPlayer(Location l, boolean survival_only) {
		List<Player> players = new ArrayList<>();
		ListIterator<Player> pit = l.getWorld().getPlayers().listIterator();
		pit.forEachRemaining(consumer -> {
			if(!survival_only || consumer.getGameMode() == GameMode.ADVENTURE || consumer.getGameMode() == GameMode.SURVIVAL)
				players.add(consumer);
		});
		if (players.size() <= 0)
			return null;

		Player p = null;
		for(Player t : players)
			if(p == null || t.getLocation().distance(l) < p.getLocation().distance(l))
				p = t;
		return p;
	}

	public static List<Player> getNearbyPlayers(Location l, double distance,
			boolean survival_only) {
		List<Player> players = new ArrayList<Player>();
		for(Player p : l.getWorld().getPlayers())
			if(survival_only || p.getGameMode() == GameMode.SURVIVAL)
				if(LocationTool.withinDistance(l, p.getLocation(), distance))
					players.add(p);
		return players;
	}

	public static LivingEntity getClosestLivingEntity(Location l, double distance){
		LivingEntity result = null;
		for(LivingEntity le : getNearbyLivingEntities(l, distance)){
			if(result == null || l.distance(result.getLocation()) > l.distance(le.getLocation()))
				result = le;
		}
		
		return result;
	}
	
	public static LivingEntity getClosestLivingEntity(Location l, double distance, LivingEntity... exclude){
		LivingEntity result = null;
		for(LivingEntity le : getNearbyLivingEntities(l, distance, exclude)){
			if(result == null || l.distance(result.getLocation()) > l.distance(le.getLocation()))
				result = le;
		}
		
		return result;
	}
	
	public static List<Entity> getEntitiesLike(World w, Entity...e){
		HashSet<Class<? extends Entity>> classes = new HashSet<Class<? extends Entity>>();
		for(Entity es : e){
			classes.add(es.getClass());
		}
		List<Entity> entities = new ArrayList<Entity>();
		for(Entity es : w.getEntities()){
			for(Class<? extends Entity> c : classes)
				if(c.isInstance(es))
					entities.add(es);
		}
		return entities;
	}

	@SafeVarargs
	public static List<Entity> getNearbyEntities(Location l, double distance, Class<? extends Entity>... c) {
		double x = l.getX();
		double y = l.getY();
		double z = l.getZ();
		
		AxisAlignedBB box = AxisAlignedBB.a(x-distance, y-distance, z-distance, x+distance, y+distance, z+distance);
		
		List<net.minecraft.server.v1_8_R3.Entity> mcList = ((CraftWorld) l.getWorld()).getHandle().getEntities(null, box);
		
		List<Entity> list = Lists.newArrayList();
		for(net.minecraft.server.v1_8_R3.Entity entity : mcList){
			Entity e = entity.getBukkitEntity();
			for (Class<? extends Entity> cl : c){
				if (e.getClass().isInstance(cl)){
					list.add(e);
					break;
				}
			}
		}
		return list;
	}
	
	@SafeVarargs
	public static List<Entity> getEntities(World w, Class<? extends Entity>...c) {
		List<Entity> entities = new ArrayList<Entity>();
		for (Entity e : w.getEntities())
			for (Class<? extends Entity> cl : c)
				if (e.getClass().isInstance(cl))
					entities.add(e);
		return entities;
	}
	
	public static List<LivingEntity> getNearbyLivingEntities(Location l,double distance) {
		double x = l.getX();
		double y = l.getY();
		double z = l.getZ();
		
		AxisAlignedBB box = AxisAlignedBB.a(x-distance, y-distance, z-distance, x+distance, y+distance, z+distance);
		
		List<net.minecraft.server.v1_8_R3.Entity> mcList = ((CraftWorld) l.getWorld()).getHandle().getEntities(null, box);
		
		List<LivingEntity> list = Lists.newArrayList();
		for(net.minecraft.server.v1_8_R3.Entity entity : mcList){
			Entity bukkit = entity.getBukkitEntity();
			if(bukkit instanceof LivingEntity) list.add((LivingEntity) bukkit); 
		}
		
		return list;
	}

	public static List<LivingEntity> getNearbyLivingEntities(Location l, double distance, LivingEntity...exclude) {
		List<LivingEntity> list = getNearbyLivingEntities(l, distance);
		if(exclude == null || exclude.length == 0) return list;
		
		Iterator<LivingEntity> iter = list.iterator();
		while(iter.hasNext()){
			int id = iter.next().getEntityId();
			for(LivingEntity e : exclude){
				if(e != null && e.getEntityId() == id){
					iter.remove();
					break;
				}
			}
		}
		
		return list;
	}
	
	/**
	 * Forces given entity to look in a certain direction.
	 * 
	 * @param e
	 *            Entity being forced to change view direction.
	 * @param l
	 *            Location that given Entity must face.
	 */
	public static void entityLookAt(Entity e, Location l) {
		Vector direction = l.toVector().subtract(getVector(e)).normalize();
		double x = direction.getX();
		double y = direction.getY();
		double z = direction.getZ();

		Location changed = l.clone();
		changed.setYaw(180 - toDegree(Math.atan2(x, z)));
		changed.setPitch(90 - toDegree(Math.acos(y)));
		e.teleport(changed);
	}

	/**
	 * Simulates the first location facing the second.
	 * 
	 * @param e
	 *            Location to be changed.
	 * @param l
	 *            Location to be faced.
	 * @return Returns the first location, but sets Yaw and Pitch to look at
	 *         second given Location.
	 */
	public static Location locationLookAt(Location e, Location l) {
		Vector direction = l.toVector().subtract(e.toVector()).normalize();
		double x = direction.getX();
		double y = direction.getY();
		double z = direction.getZ();

		Location changed = l.clone();
		changed.setYaw(180 - toDegree(Math.atan2(x, z)));
		changed.setPitch(90 - toDegree(Math.acos(y)));
		return changed;
	}

	/**
	 * Converts angle to degrees.
	 * 
	 * @param angle
	 *            Angle to convert.
	 * @return Returns the angle converted to degrees.
	 */
	private static float toDegree(double angle) {
		return (float) Math.toDegrees(angle);
	}

	public static Vector getDirectionVector(LivingEntity le) {
		return getDirectionVector(le.getEyeLocation().getYaw(), le
				.getEyeLocation().getPitch());
	}

	public static Vector getDirectionVector(float yaw, float pitch) {
		Vector v = new Vector();
		v.setX(-(float) sin(toRadians(yaw)) * (float) cos(toRadians(pitch)));
		v.setY(-(float) sin(toRadians(pitch)));
		v.setZ((float) cos(toRadians(yaw)) * (float) cos(toRadians(pitch)));
		return v;
	}
	
	/**
	 * Returns a target entity by using the players looking
	 * direction. If no entity is found this returns null.
	 * 
	 * @param le LivingEntity in question.
	 * @param iterations How many blocks to check.
	 * @return LivingEntity is returned, is null if none is found.
	 */
	public static LivingEntity getTargetEntity(LivingEntity le, int iterations) {
		Location l = le.getEyeLocation().clone();
		float yaw = l.getYaw();
		float pitch = l.getPitch();

		Vector v = getDirectionVector(yaw, pitch);
		double i = 0;
		while (i < iterations) {
			i++;
			l.add(v);
			for(Entity e : l.getChunk().getEntities())
				if(e instanceof LivingEntity) {
					LivingEntity lee = (LivingEntity) e;
					if(lee == le)
						continue;
					if(lee.getLocation().distance(l) < 1 || lee.getEyeLocation().distance(l) < 1)
						return lee;
				}
		}

		return null;
	}

	/**
	 * Returns random velocity.
	 * 
	 * @param multiplier
	 *            scales the velocity.
	 * @return returns a random vector.
	 */
	public static Vector randomVelocity(float multiplier) {
		return new Vector(Math.random() - Math.random(), Math.random()
				- Math.random(), Math.random() - Math.random())
		.multiply(multiplier);
	}

	/**
	 * Returns random velocity between a range.
	 * 
	 * @param multiplier
	 *            scales the velocity.
	 * @return returns a random vector.
	 */
	public static Vector randomVelocityRange(float min, float max) {
		float multiplier = max - min;
		Vector maxv = new Vector(Math.random() - Math.random(), Math.random()
				- Math.random(), Math.random() - Math.random())
		.multiply(multiplier);
		return new Vector(min, min, min).add(maxv);
	}
	
	/**
	 * Returns the .toVector() of an entities eye location, if not found,
	 * returns location.
	 * 
	 * @param e
	 *            Given Entity to get .toVector() location from.
	 * @return Returns Vector variable from entity.
	 */
	private static Vector getVector(Entity e) {
		if (e instanceof Player)
			return ((Player) e).getEyeLocation().toVector();
		else
			return e.getLocation().toVector();
	}

	/**
	 * Returns a normalized velocity with a vector pointing from one location
	 * to the other.
	 * @param vectorFrom origin location 
	 * @param vectorTo location to point towards
	 */
	public static Vector getVector(Location vectorFrom, Location vectorTo) {
		return locationLookAt(vectorFrom.clone(), vectorTo.clone()).getDirection();
	}
	
	
	/**
	 * Spawns a firework effect in the specified location.
	 * Simplified version that makes a colored standard ball firework.
	 * 
	 * Depending on lag severity, players may see the firework object pre-detonated.
	 * Firework sounds are always heard.
	 * 
	 * @param location location to make effect in.
	 * @param color color of the firework.
	 */
	public static void displayFirework(Location location, Color color) {
		Firework firework = location.getWorld().spawn(location, Firework.class);
		FireworkMeta fMeta = firework.getFireworkMeta();
		fMeta.addEffect(FireworkEffect.builder().withColor(color).build());
		firework.setFireworkMeta(fMeta);
		firework.detonate();
	}

	/**
	 * Spawns a firework effect in the specified location.
	 * 
	 * Depending on lag severity, players may see the firework object pre-detonated.
	 * Firework sounds are always heard.
	 * 
	 * @param location location to make effect in.
	 * @param effect use FireworkEffect.builder() and build() after setting options for maximum efficiency.
	 */
	public static void displayFirework(Location location, FireworkEffect effect) {
		Firework firework = location.getWorld().spawn(location, Firework.class);
		FireworkMeta fMeta = firework.getFireworkMeta();
		fMeta.addEffect(effect);
		firework.setFireworkMeta(fMeta);
		firework.detonate();
	}
}
