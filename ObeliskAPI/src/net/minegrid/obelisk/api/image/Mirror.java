package net.minegrid.obelisk.api.image;

import net.minegrid.obelisk.core.image.CoreInventoryImage;
import net.minegrid.obelisk.core.image.CoreItemImage;
import net.minegrid.obelisk.core.image.CorePlayerInventoryImage;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Maxim on 2016-02-25.
 */
public interface Mirror {
    static Image.Item imageItem(ItemStack item) {
        return new CoreItemImage(item);
    }

    static Image.Inventory imageInventory(Inventory inv) {
        return new CoreInventoryImage(inv);
    }

    static Image.PlayerInventory imagePlayerInventory(Player player) {
        return new CorePlayerInventoryImage(player);
    }
}
