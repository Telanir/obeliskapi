package net.minegrid.obelisk.api.image;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Maxim on 2016-02-25.
 */
public interface Image {
    interface Inventory {
        void apply(org.bukkit.inventory.Inventory inventory);
        Item[] getItems();
    }

    interface PlayerInventory {
        void apply(Player player);
        Item[] getItems();
        Item[] getArmor();
    }

    interface Item {
        ItemStack toItem();
    }
}
