package net.minegrid.obelisk.api;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import java.io.PrintStream;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Maxim on 2016-02-20.
 */
public abstract class MonitorSender {
    Monitor rank;

    public MonitorSender(Monitor rank) {
        this.rank = rank;
    }

    public Monitor getRank() {
        return rank;
    }

    public abstract void send(String msg);

    public void terminate() {}

    public static class Broadcast extends MonitorSender {
        boolean players;
        boolean console;

        public Broadcast(Monitor rank) {
            super(rank);
            this.players = true;
            this.console = true;
        }

        public Broadcast(Monitor rank, boolean players, boolean console) {
            super(rank);
            this.players = players;
            this.console = console;
        }

        @Override
        public void send(String msg) {
            if(console)
                Bukkit.getConsoleSender().sendMessage(msg);
            if(players)
                Bukkit.getOnlinePlayers().forEach(player -> player.sendMessage(msg));
        }

        public static Broadcast build(Monitor rank) {
            return new Broadcast(rank);
        }

        public static Broadcast build(Monitor rank, boolean players, boolean console) {
            return new Broadcast(rank, players, console);
        }
    }

    public static class Single extends MonitorSender {
        CommandSender sender;

        public Single(Monitor rank, CommandSender sender) {
            super(rank);
            this.sender = sender;
        }

        public CommandSender getSender() {
            return sender;
        }

        public void setSender(CommandSender sender) {
            this.sender = sender;
        }

        @Override
        public void send(String msg) {
            if(sender != null)
                sender.sendMessage(msg);
        }

        public static Single build(Monitor rank, CommandSender sender) {
            return new Single(rank, sender);
        }
    }

    public static class Multi extends MonitorSender {
        Set<CommandSender> senders;

        public Multi(Monitor rank) {
            super(rank);
            this.senders = new HashSet<>();
        }

        public Multi(Monitor rank, CommandSender...senders) {
            this(rank);
            for(CommandSender s : senders)
                addSender(s);
        }

        public Collection<CommandSender> getSenders() {
            return Collections.unmodifiableCollection(senders);
        }

        public void addSender(CommandSender sender) {
            if(!senders.contains(sender))
                senders.add(sender);
        }

        public void removeSender(CommandSender sender) {
            senders.remove(sender);
        }

        public void clear() {
            senders.clear();
        }

        @Override
        public void send(String msg) {
            if(msg != null)
                senders.forEach(sender -> sender.sendMessage(msg));
        }

        public static Multi build(Monitor rank) {
            return new Multi(rank);
        }

        public static Multi build(Monitor rank, CommandSender...senders) {
            return new Multi(rank, senders);
        }
    }

    public static class Group extends MonitorSender {
        Collection<CommandSender> senders;

        public Group(Monitor rule, Collection<CommandSender> senders) {
            super(rule);
            this.senders = senders;
        }

        public Collection<CommandSender> getCollection() {
            return senders;
        }

        public void setCollection(Collection<CommandSender> senders) {
            this.senders = senders;
        }

        @Override
        public void send(String msg) {
            if(senders != null && msg != null)
                senders.forEach(sender -> sender.sendMessage(msg));
        }

        public static Group build(Monitor rank, Collection<CommandSender> senders) {
            return new Group(rank, senders);
        }
    }

    public static class File extends MonitorSender {
        java.io.File file;
        PrintStream out;

        public File(Monitor rule, java.io.File file) {
            super(rule);
            this.file = file;
            try {
                out = new PrintStream(file);
            } catch(Exception e) { e.printStackTrace(); }
        }

        public java.io.File getFile() {
            return file;
        }

        public PrintStream getStream() {
            return out;
        }

        @Override
        public void send(String msg) {
            if(out != null && msg != null)
                out.println(msg);
        }

        @Override
        public void terminate() {
            if(out != null)
                out.close();
        }

        public static File build(Monitor rule, java.io.File file) {
            return new File(rule, file);
        }
    }
}