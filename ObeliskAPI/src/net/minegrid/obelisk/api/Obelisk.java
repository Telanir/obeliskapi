package net.minegrid.obelisk.api;

import java.util.UUID;

import net.minegrid.obelisk.api.menu.active.MenuSession;
import net.minegrid.obelisk.api.menu.enums.SessionState;
import net.minegrid.obelisk.core.menu.MenuDaemon;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import net.minegrid.obelisk.api.command.GlobalParam;
import net.minegrid.obelisk.api.command.ObeliskListener;
import net.minegrid.obelisk.api.command.ParseElement;
import net.minegrid.obelisk.api.command.ParseElement.ParseAction;
import net.minegrid.obelisk.api.command.OCmdRuntime;
import net.minegrid.obelisk.core.Foundation;
import net.minegrid.obelisk.core.FoundationTask;
import net.minegrid.obelisk.core.agents.CommandHandler;
import net.minegrid.obelisk.core.agents.CoreParseElement;

/**
 * <code>Obelisk</code> is the API that speaks to the <code>Foundation</code> framework.
 * Obelisk provides the following to enhance and ease the development process:
 * <ul>
 * <li>Block-API</li>
 * <li>Command-API</li>
 * <li>MenuOld-API</li>
 * <li>File-API</li>
 * <li>Development Tools {@link net.minegrid.obelisk.api.tools}</li>
 * <li>Particle/Reflection/Attribute libraries thanks to their respective developers.</li>
 * </ul>
 * 
 * @author Maxim Chipeev
 * @version 1.0
 * @since 1.0.0
 */
public interface Obelisk {
	/**
	 * Prompt the Command-API to register all commands found in the specified class.
	 * @see net.minegrid.obelisk.api.command.OCmd
	 * @param listener This class should have {@code OCmd} objects.
	 * @param module Module that this class belongs to.
	 */
    static void registerCommands(ObeliskListener listener, JavaModule module) {
    	CommandHandler.getCommand().register(listener, module);
    }
    
    /**
     * Unregister all commands belonging to the listener.
     * @see net.minegrid.obelisk.api.command.OCmd
     * @param listener This class should have <code>OCmd</code> objects.
     */
	static void unregisterCommands(ObeliskListener listener) {
		CommandHandler.getCommand().unregister(listener);
	}
	
	/**
	 * Unregister all <code>ObeliskListener</code> commands related to the specified <code>Module</code>.
	 * @param m Module to unregister.
	 */
	static void unregisterCommands(Module m) {
		CommandHandler.getCommand().unregister(m);
	}
	
	/**
	 * Unregister the command that matches the key exactly.
	 * @param cmd Key to match. Must include wildcards and etc.
	 */
	static void unregisterCommand(String cmd) {
		CommandHandler.getCommand().unregister(cmd);
	}
    
	/**
	 * Returns a User object for the specified player, guaranteed to provide User file.
	 * @param p Non-null online player.
	 * @return User object with data relevant to the specified player.
	 */
    static User getUser(Player p) {
    	return Foundation.getPlayerManager().getUser(p);
    }
    
    /**
     * Returns a User object for the specified UUID.
     * @param u UUID to use.
     * @return null if file corrupted or UUID has never logged in.
     */
    static User getUser(UUID u) {
    	return Foundation.getPlayerManager().getUser(u);
    }
    
    /**
     * Returns whether or not the specified player is currently accessing a
     * <code>Foundation</code> supported <code>MenuOld</code>.
     * 
     * @see MenuSession
     * @param p Player to use in query.
     * @return true if a MenuOld object is linked to this player.
     */
    static boolean inMenu(Player p) {
    	return MenuDaemon.isUsingMenu(p);
    }
    
    /**
     * Returns the active <code>MenuSession</code> for the specified player.
     * 
     * @see MenuSession
     * @param p Player to use in query.
     * @return null if there is no attached session.
     */
    static MenuSession getMenu(Player p) {
        if(!MenuDaemon.isUsingMenu(p)) return null;
    	return MenuDaemon.getStatus(p).getSession();
    }
    
    /**
     * Returns the current <code>SessionState</code> for a given player.
     * 
     * @see SessionState
     * @param p Player to use in query.
     * @return <code>SessionState.DETACHED</code> if there is no session.
     */
    static SessionState getState(Player p) {
    	if(!inMenu(p))
    		return SessionState.CLOSED;
    	return MenuDaemon.getStatus(p).getState();
    }
    
    /**
     * Terminates any <code>MenuSession</code> currently attached to the player.
     * 
     * @see MenuSession
     * @param p Player to use in query.
     */
    static void killSession(Player p) {
    	if(!inMenu(p))
    		return;
    	MenuDaemon.getStatus(p).getSession().setState(p, SessionState.CLOSED);
    }
    
    /**
     * Registers a global parameter.
     * <pre> ie)
     * {@code}
     * Obelisk.registerGlobalParameter(this, "h", (GlobalParam.Input) -> {
     *   // code here
     * });
     * </pre>
     * 
     * @see net.minegrid.obelisk.api.command.GlobalParam
     * @param m Owning module.
     * @param tag The dash '-' is automatically prepended.
     * @param code Code to execute when the parameter is used.
     * @return returns the constructed <code>GlobalParam</code>.
     */
    static GlobalParam registerGlobalParameter(Module m, String tag, GlobalParam.Execution code) {
    	GlobalParam gp = new GlobalParam(tag, code);
    	Foundation.getCommandHandler().registerGlobalParameter(gp, m);
    	return gp;
    }
    
    /**
     * Registers a global parameter that requires a trailing value.
     * <pre> ie)
     * {@code}
     * Obelisk.registerGlobalParameter(this, "h", "%i",(GlobalParam.Input) -> {
     *   // code here
     * });
     * </pre>
     * 
     * @param m Owning module.
     * @param tag The dash '-' is automatically prepended.
     * @param parse The required type of trailing input. {@code %s} is any value.
     * @param code Code to execute when the parameter is used.
     * @return returns the constructed {@code GlobalParam}
     * @see net.minegrid.obelisk.api.command.GlobalParam
     */
    static GlobalParam registerGlobalParameter(Module m, String tag, String parse, GlobalParam.Execution code) {
    	GlobalParam gp = new GlobalParam(tag, parse, code);
    	Foundation.getCommandHandler().registerGlobalParameter(gp, m);
    	return gp;
    }
    
    /**
     * Removes a global parameter from service.
     * @param tag Identifying tag.
     */
    static void unregisterGlobalParameter(String tag) {
    	Foundation.getCommandHandler().unregisterGlobalParameter(tag);
    }
    
    /**
     * Registers a value parser. ParseAction will receive a String argument that
     * it must parse. Returning the same argument is a successful parse, returning
     * null will indicate a failure and send the error message.
     * <pre>ie)
     * {@code}
     * Obelisk.registerParseElement("Decimal", "%d", "% is not a decimal value!", (String value) -> {
     *   // parse value
     *   return value;
     * });
     * </pre>
     * 
     * @param name Display name of the element. eg) {@code Decimal}
     * @param parse Parse identifier. eg) {@code %d}
     * @param errorMessage Error message when value is incorrect.
     * @param action Executed to determine proper syntax in a command.
     * @return returns the constructed {@code ParseElement}
     * @see net.minegrid.obelisk.api.command.ParseElement
     */
    static ParseElement registerParseElement(String name, String parse, String errorMessage, ParseAction action) {
    	CoreParseElement cpe = new CoreParseElement(name.toLowerCase(), parse, errorMessage, action);
    	Foundation.getCommandHandler().registerParse(cpe);
    	return cpe;
    }
    
    /**
     * Registers a repeating task at the specified interval with the tag.
     * ObeliskTasks are automatically canceled when their respective {@code Module}
     * is disabled.
     * 
     * @param module Owning {@code Module}
     * @param tag Identifying tag.
     * @param interval Interval in ticks between each execution.
     * @param run Code to execute.
     * @return Returns the constructed {@code ObeliskTask}
     */
    static ObeliskTask registerTask(Module module, String tag, long interval, Runnable run) {
    	FoundationTask.CoreTask ct = new FoundationTask.CoreTask(module, interval, run);
    	FoundationTask.registerTask(tag, ct);
    	return ct;
    }
    
    /**
     * Deletes the {@code ObeliskTask} that is using the specified tag.
     * @param tag Identifying tag.
     */
    static void stopTask(String tag) {
    	FoundationTask.stopRunnable(tag);
    }
    
    /**
     * Use this to determine if a certain task is still running.
     * @param tag Identifying tag.
     * @return true if a task is registered under the tag.
     */
    static boolean taskExists(String tag) {
    	return FoundationTask.exists(tag);
    }

    /**
     * Returns all registered OCmd's.
     * Runtime commands cannot be modified but can be read, they represent
     * a runtime clone of all command parameters.
     * 
     * @return Immutable list of runtime commands.
     */
    static OCmdRuntime[] getRegisteredCommands() {
    	return Foundation.getCommandHandler().getCommands();
    }
    
    /**
     * Returns all registered OCmd's under the specified base-command.
     * Runtime commands cannot be modified but can be read, they represent
     * a runtime clone of all command parameters.
     * 
     * @return Immutable list of runtime commands.
     */
    static OCmdRuntime[] getRegisteredCommands(String baseCommand) {
    	return Foundation.getCommandHandler().getCommands(baseCommand);
    }
    
    /**
     * Disables the specified module.
     * @param m Module to disable.
     * @return Result of the action.
     */
    static Module.StateChangeResult disable(Module m) {
        return Foundation.getDaemon().disable(m);
    }

    /**
     * Disables the specified module.
     * @param m Module to disable.
     * @param force If true, will also disable all dependent modules.
     * @return Result of the action.
     */
    static Module.StateChangeResult disable(Module m, boolean force) {
        return Foundation.getDaemon().disable(m, force);
    }

    /**
     * Enables the specified module.
     * @param m Module to enable.
     * @return Result of the action.
     */
    static Module.StateChangeResult enable(Module m) {
        return Foundation.getDaemon().enable(m);
    }

    /**
     * Enables the specified module.
     * @param m Module to enable.
     * @param force If true, will also enable all dependencies of the module.
     * @return Result of the action.
     */
    static Module.StateChangeResult enable(Module m, boolean force) {
        return Foundation.getDaemon().enable(m, force);
    }

    /**
     * Saves the {@link Module} or its {@link net.minegrid.obelisk.api.Module.DataClump} to disk.
     * @param m Module to save.
     */
    static void saveModule(Module m) {
        Foundation.getDaemon().saveModule(m);
    }
    
    /**
     * Returns the specified {@link Module}, applying the specified arguments as necessary.
     * @param module Class of the {@link Module} to grab.
     * @param args {@code ENABLE} will make sure the returned {@code Module} is enabled, {@code FORCE} will ensure this is true.
     * @return null if {@code Module} could not be found.
     */
    @SuppressWarnings("unchecked")
	static <T extends Module> T getModule(Class<T> module, ModuleGetArgument...args) {
        return (T)Foundation.getDaemon().getModule(module, args);
    }

    /**
     * Returns the specified {@link Module}, applying the specified arguments as necessary.
     * @param name Name of the {@link Module} to grab.
     * @param args {@code ENABLE} will make sure the returned {@code Module} is enabled, {@code FORCE} will ensure this is true.
     * @return null if {@code Module} could not be found.
     */
    static Module getModule(String name, ModuleGetArgument...args) {
        return Foundation.getDaemon().getModule(name, args);
    }

    /**
     * Returns the core rule for Foundation.
     * Obelisk sends all internal messages to the core rule.
     * @see net.minegrid.obelisk.api.Monitor.Rule
     */
    static Monitor.Rule getCoreRule() {
        return Foundation.getCoreRule();
    }
    
    /**
     * Registers Bukkit-events for the specified Listener.
     * @param el Listener to register.
     */
    static void registerEvents(Listener el) {
    	Bukkit.getPluginManager().registerEvents(el, getPlugin());
    }
    
    /**
     * Returns the control-plugin <code>Foundation</code> that manages all modules.
     * @return <code>Foundation</code>
     */
    static JavaPlugin getPlugin() {
    	return Foundation.getInstance();
    }
    
    public enum ModuleGetArgument {
        FORCE, ENABLE;
    }
}
